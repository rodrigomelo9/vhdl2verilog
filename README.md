# vhdl2verilog

A free software ([GPL3](LICENSE)) compiler from synthesizable VHDL (93) to Verilog (2001).

## Roadmap

* Support for the full VHDL93 grammar, without Verilog translation
* Support for structural descriptions
* Support for common synthesizable constructions
* Support for more advanced synthesizable constructions
* Support for simulation constructions
* Support for other things when needed
