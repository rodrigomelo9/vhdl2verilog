library IEEE;
use IEEE.std_logic_1164.all;
library STD;
use STD.textio.all;

entity rem_vs_mod is
end entity rem_vs_mod;

architecture behav of rem_vs_mod is
   procedure display(message: string) is
      variable l : line;
   begin
      write(l,message);
      writeline(output,l);
   end procedure display;
begin
   process
      variable l : line;
      variable a, b: integer;
   begin
      display("a b rem mod");
      for j in 1 to 9 loop
          for i in 1 to 9 loop
              if i /= j then
                 a := i; b := j;
                 display(integer'image(a)&" "&integer'image(b)&" "&integer'image(a rem b)&" "&integer'image(a mod b));
                 a := i; b := -j;
                 display(integer'image(a)&" "&integer'image(b)&" "&integer'image(a rem b)&" "&integer'image(a mod b));
                 a := -i; b := j;
                 display(integer'image(a)&" "&integer'image(b)&" "&integer'image(a rem b)&" "&integer'image(a mod b));
                 a := -i; b := -j;
                 display(integer'image(a)&" "&integer'image(b)&" "&integer'image(a rem b)&" "&integer'image(a mod b));
              end if;
          end loop;
      end loop;
      wait;
   end process;
end architecture behav;
