module rem_vs_mod ();

   reg signed [31:0] a, b, i, j;

   function mod;
      input c, d;
   begin
     mod = ( (c%d!=0) && ( (c<0 && d>=0) || (c>=0 && d<0) ) ) ? c-(d*(c/d - 1)) : c % d;
           // floor(n) = n - (n%1)
   end
   endfunction

   initial begin
      $display("a b rem mod");
      for (j=1 ; j<=9 ; j=j+1) begin
          for (i=1 ; i<=9 ; i=i+1) begin
              if (i!=j) begin
                 a = i; b = j;
                 $display("%1d %1d %1d %1d",a,b,a%b,mod(a,b));
                 a = i; b = -j;
                 $display("%1d %1d %1d %1d",a,b,a%b,mod(a,b));
                 a = -i; b = j;
                 $display("%1d %1d %1d %1d",a,b,a%b,mod(a,b));
                 a = -i; b = -j;
                 $display("%1d %1d %1d %1d",a,b,a%b,mod(a,b));
              end
          end
      end
      $finish();
   end

endmodule
