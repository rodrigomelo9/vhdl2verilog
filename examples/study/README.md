# Study cases

This directory contains:
* Simulation files to evaluate how to compile some VHDL constructions into Verilog.
* Synthesis files (*_syn*) to evaluate the generated hardware.

Use `make test` to analyze and run simulations.
