# Examples

* [coverage](coverage): used to exercise all the syntax rules, but does not necessarily has sense.
* [simulation](simulation): used to compare that the VHDL and Verilog versions generates the same outputs.
* [synthesis](synthesis): used to ensure that the VHDL and Verilog versions generates the same hardware.
