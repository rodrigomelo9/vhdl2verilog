library IEEE;
use IEEE.std_logic_1164.all;

entity Types is
end entity Types;

architecture Types_Arch of Types is

   -- Incomplete type declarations are ignored
   type type1;

   -- Enumerations
   type type1 is (STATE1, STATE2, STATE3, STATE4);
   type type2 is (STATEA, STATEB, STATEC, STATED, STATEE, STATEF, STATEG, STATEH, STATEI, STATEJ);

   -- Arrays
   -- type type_name is array (specification) of data_type;
   -- signal signal_name: type_name [:= initial_value];
   -- type array1 is array (0 to 3) of std_logic;
   -- type array2 is array (3 downto 0) of std_logic_vector(7 downto 0);
   -- type array3 is array (natural range <>) of std_logic_vector(7 downto 0);
   -- type array4 is array (0 to 9, 0 to 7) of std_logic_vector(7 downto 0);

   ----------------------------------------------------------------------------
   -- Unsupported
   ----------------------------------------------------------------------------

   -- User defined Integer data types
   type typeU1 is range 0 to 32;
   type typeU2 is range -32 to 32;

   -- Physical types
   type distance is range 0 to 1e6
      units
         um;
         mm = 1000 um;
         m  = 1000 mm;
      end units;

   -- Records
   type birthday is record
      day:   integer;
      month: string(1 to 10);
   end record;

--   -- Subtypes
--   subtype index is integer range 0 to 7;
--   subtype digit is character range '0' to '9';
   subtype id    is string(1 to 15);
   subtype addr  is std_logic_vector(31 downto 0);

   signal aux1 : distance;
   signal aux2 : id;
   signal aux3 : addr;

begin
   aux1 <= 45 mm;
end architecture Types_Arch;
