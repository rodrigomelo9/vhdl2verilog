library IEEE;
use IEEE.std_logic_1164.all;

entity Clock is
   port (
      clk1_i, clk2_i, rst_i : in std_logic
   );
end entity Clock;

architecture Clock_Arch of Clock is

begin

   sync1: process (clk1_i)
   begin
      if rising_edge(clk1_i) then
         if rst_i = '1' then
         else
         end if;
      end if;
   end process sync1;

--   sync2: process (clk1_i)
--   begin
--      if clk1_i'event and clk1_i = '1' then
--      end if;
--   end process sync2;

--   sync3: process (clk1_i)
--   begin
--      if clk1_i'event and clk1_i = '0' then
--      end if;
--   end process sync3;

   async1: process (clk1_i, rst_i)
   begin
      if rst_i = '1' then
      else
         if rising_edge(clk1_i) then
         end if;
      end if;
   end process;

   async2: process (clk1_i, rst_i)
   begin
      if rst_i = '1' then
      elsif rising_edge(clk1_i) then
      end if;
   end process;

   process (clk1_i, clk2_i)
   begin
      if rising_edge(clk1_i) then
      end if;
      if falling_edge(clk2_i) then
      end if;
   end process;

end architecture Clock_Arch;
