configuration CONFIGURATION1 of ENTITY1 is
   use work.all;
   for ARCHITECTURE1
       for INST1: COMP1
           use configuration WORK.CONFIGURATION2;
       end for;
       for INST2, INST3, INST4: COMP2
           use entity WORK.COMP2(STRUCTURAL);
       end for;
       for all: COMP3
           -- use defaults
       end for;
    end for;
end configuration CONFIGURATION1;

entity Unsupported is
end entity Unsupported;

architecture Unsupported_Arch of Unsupported is

   group PIN2PIN is (signal, signal);
   group RESOURCE is (label <>);
   group DIFF_CYCLES is (group <>);
   --
--   group G1: RESOURCE (L1, L2);
--   group G2: RESOURCE (L3, L4, L5);
--   group C2Q: PIN2PIN (PROJECT.GLOBALS.CK, Q);
--   group CONSTRAINT1: DIFF_CYCLES (G1, G3);

begin
end;
