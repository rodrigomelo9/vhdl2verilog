library IEEE;
use IEEE.std_logic_1164.all;

entity Statements is
end entity Statements;

architecture Statements_Arch of Statements is

   signal result : std_logic_vector(7 downto 0);
   signal op     : natural;

begin

   result <= x"A9" when op = 1 else
             x"0C" when op = 2 else
             x"1F" when op = 4 else (others => '0');

   with op select result <= x"A9" when 1,
                            x"0C" when 2,
                            x"1F" when 4,
                            (others => '0') when others;

   process(op)
   begin
      -- Full
      if op = 1 then
         result <= x"A9";
      elsif op = 2 then
         result <= x"0C";
      elsif op = 4 then
         result <= x"1F";
      else
         result <= (others => '0');
      end if;
      -- Incomplete
      labeled_if : if op = 7 then
         result <= (others => '1');
      end if;
   end process;

   proc : process(op)
   begin
      -- Full
      case op is
         when 0      => result <= x"00";
         when 1 | 2  => result <= x"12";
         when 3 | 4  => result <= x"34";
         when 5 to 7 => result <= (others => '1');
         when others => null;
      end case;
   end process;

   nested : process(op, result)
   begin
      if op = 1 then
         if result /= x"A9" then
            result <= x"A9";
         else
            labeled : result <= result;
         end if;
      else
         case op is
            when 2          => result <= x"22";
            when 3          => result <= x"33";
            when 6 downto 4 => result <= x"44";
            when others     => result <= x"55";
         end case;
      end if;
      --
      case op is
         when 0 =>
            if result /= x"A9" then
               result <= x"A9";
            else
               result <= result;
            end if;
         when others =>
            case op is
               when 2 => result <= x"22";
               when 3 => result <= x"33";
               when others => result <= x"44";
            end case;
      end case;
   end process;

end architecture Statements_Arch;
