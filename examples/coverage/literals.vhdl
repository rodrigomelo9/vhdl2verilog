entity Literals is
end entity Literals;

architecture Literals_Arch of Literals is

   -- abstract_literal ::= decimal_literal | based_literal
   constant CONSTANT01 : integer := 1234;
   constant CONSTANT02 : integer := 1_234;
   constant CONSTANT03 : integer := 1E6;
   constant CONSTANT04 : integer := 1e+6;
   constant CONSTANT05 : real    := 1.234;
   constant CONSTANT06 : real    := 1.234_567;
   constant CONSTANT07 : real    := 1.0E-6;
   constant CONSTANT08 : real    := 1.0E+6;

   -- based_literal ::= base # based_integer [ . based_integer ] # [ exponent ]
   constant CONSTANT09 : integer := 2#1111_1111#;
   constant CONSTANT10 : integer := 16#FF#;
   constant CONSTANT11 : integer := 016#0FF#;
   constant CONSTANT12 : integer := 16#E#E1;
   constant CONSTANT13 : integer := 2#1110_0000#;
   constant CONSTANT14 : real    := 16#F.FF#E+2;
   constant CONSTANT15 : real    := 2#1.1111_1111_111#E11;
   constant CONSTANT16 : integer := 11#02468A#;

   -- character_literal ::= ' graphic_character '
   constant CONSTANT17 : character := 'A';
   constant CONSTANT18 : character := '*';
   constant CONSTANT19 : character := ''';
   constant CONSTANT20 : character := ' ';

   -- string_literal ::= " { graphic_character } "
   constant CONSTANT21 : string    := "Setup time is too short";
   constant CONSTANT22 : string    := "";   -- An empty string literal.
   constant CONSTANT23 : string    := " ";  -- A string literals of length 1
   constant CONSTANT24 : string    := "A";  -- A string literals of length 1
   constant CONSTANT25 : string    := """"; -- A string literals of length 1

begin
end architecture Literals_Arch;
