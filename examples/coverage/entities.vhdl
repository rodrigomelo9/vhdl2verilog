entity ENTITY1 is
   port ( -- Modes
      port1 :         bit;
      port2 :      in bit;
      port3 :     out bit;
      port4 :   inout bit;
      port5 :  buffer bit;
      port6 : linkage bit
   );
end entity ENTITY1;

-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ENTITY2 is
   generic (
      GENERIC1 : in integer;
      GENERIC2 : natural;
      GENERIC3 : positive;
      GENERIC4 : bit;
      GENERIC5 : bit_vector(0 to 7);
      GENERIC6 : std_logic;
      GENERIC7 : std_logic_vector(7 downto 0)
   );
   port ( -- Bits types/subtypes
      port1 : in integer;
      port2 : in natural;
      port3 : in positive;
      port4 : in std_logic;
      port5 : in std_ulogic;
      port6 : in boolean
   );
end entity ENTITY2;

-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ENTITY3 is
   port ( -- For simulation
      port1 : out real;
      port2 : out time
   );
end entity ENTITY3;

-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all, IEEE.numeric_std.all;

entity ENTITY4 is
   port ( -- Vectors types/subtypes
      port1 : in bit_vector(0 to 7);
      port2 : in std_logic_vector(7 downto 0);
      port3 : in std_ulogic_vector(5 to 10);
      port4 : in signed(10 downto 5);
      port5 : in unsigned(10 downto 5)
   );
end entity ENTITY4;

-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ENTITY5 is
   generic (
      GENERIC1 : integer  := -1;
      GENERIC2 : natural  := 0;
      GENERIC3 : positive := +1;
      GENERIC4 : bit := '0';
      GENERIC5 : bit_vector(0 to 7) := "01010011";
      GENERIC6 : std_logic := '1';
      GENERIC7 : std_logic_vector(7 downto 0) := "00001111"
   );
   port ( -- Complex Vectors types/subtypes
      port1 : out std_logic_vector(GENERIC1 downto 0);
      port2 : out std_logic_vector(GENERIC1-1 downto 0);
      port3 : out std_logic_vector(GENERIC1+1 downto 0);
      port4 : out std_logic_vector(GENERIC1*2 downto 0);
      port5 : out std_logic_vector(GENERIC1/2 downto 0);
      port6 : out std_logic_vector(GENERIC1**2 downto 0)
   );
end entity ENTITY5;

-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ENTITY6 is
   generic (
      GENERIC1 : integer  := 4;
      GENERIC2 : integer  := 2**4;
      GENERIC3 : integer  := 8-1;
      GENERIC4 : integer  := 8/4+3*(9-5)
   );
   port ( -- Complex Vectors types/subtypes
      port1 : in std_logic_vector(GENERIC1/4-1 downto GENERIC1*(2+GENERIC2)-GENERIC3)
   );
end entity ENTITY6;

-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ENTITY7 is
   port ( -- assign exp
      port1 : out std_logic := '0';
      port2 : out std_logic := '1';
      port3 : out std_logic_vector(7 downto 0) := "01101001";
      port4 : out std_logic_vector(7 downto 0) := b"11000111";
      port5 : out std_logic_vector(5 downto 0) := o"17";
      port6 : out std_logic_vector(7 downto 0) := x"A9";
      port7 : out std_logic_vector(7 downto 0) := (others => '0');
      port8 : out std_logic_vector(7 downto 0) := (others => '1');
      port9 : out integer := 9
   );
end entity ENTITY7;
