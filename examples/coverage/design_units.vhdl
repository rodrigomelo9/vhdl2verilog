-- entity and one architecture

entity ENTITY_1 is
end entity ENTITY_1;

architecture ARCHITECTURE_1 of ENTITY_1 is
begin
end architecture ARCHITECTURE_1;

-- only entity

entity ENTITY_2 is
end ENTITY_2; -- no entity keyword

-- entity and more than one architecture

entity ENTITY_3 is
end entity ENTITY_3;

architecture ARCHITECTURE_1 of ENTITY_3 is
begin
end ARCHITECTURE_1; -- no architecture keyword

architecture ARCHITECTURE_2 of ENTITY_3 is
begin
end architecture; -- no identifier

architecture ARCHITECTURE_3 of ENTITY_3 is
begin
end; -- no entity keyword, no identifier

-- configuration

configuration CONFIGURE_1 of ENTITY_3 is
   for ARCHITECTURE_1
   end for;
end configuration CONFIGURE_1;

-- package

package PACKAGE_1 is
end PACKAGE_1; -- no package keyword

-- package and package_body

package PACKAGE_2 is
end package PACKAGE_2;

package body PACKAGE_2 is
end package body PACKAGE_2;
