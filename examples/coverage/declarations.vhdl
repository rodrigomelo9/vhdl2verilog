library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Declarations is
end entity Declarations;

architecture Declarations_Arch of Declarations is

   ----------------------------------------------------------------------------
   -- Constants
   ----------------------------------------------------------------------------
   constant CONSTANT1 : integer := 32;
   constant CONSTANT2 : std_logic_vector(31 downto 0) := (others => '0');
   constant CONSTANT3 : std_logic_vector(CONSTANT1-1 downto 0) := (others => '1');
   --
   constant CONSTANT4 : std_logic := '0';
   constant CONSTANT5 : signed(CONSTANT1-1 downto 0) := x"CAFE0007";
   constant CONSTANT6 : unsigned(15 downto 0) := (others => '1');
   --
   constant CONSTANT7 : integer := CONSTANT1*8/4-2**(CONSTANT1-1);

   ----------------------------------------------------------------------------
   -- Signals
   ----------------------------------------------------------------------------
   signal SIGNAL1 : std_logic;
   signal SIGNAL2 : std_logic_vector(31 downto 0);
   signal SIGNAL3 : std_logic_vector(CONSTANT1-1 downto 0);
   --
   signal SIGNAL4 : std_logic;
   signal SIGNAL5 : std_logic_vector(31 downto 0);
   --
   signal SIGNAL6 : std_logic := '0';
   signal SIGNAL7 : signed(CONSTANT1-1 downto 0) := x"CAFE0007";
   signal SIGNAL8 : unsigned(15 downto 0) := (others => '1');
   --
   signal SIGNAL9 : integer := CONSTANT1*8/4-2**(CONSTANT1-1);

   ----------------------------------------------------------------------------
   -- Variables
   ----------------------------------------------------------------------------
   shared variable SVARIABLE1 : std_logic;
   shared variable SVARIABLE2 : std_logic_vector(31 downto 0);
   shared variable SVARIABLE3 : std_logic_vector(CONSTANT1-1 downto 0);
   --
   shared variable SVARIABLE4 : std_logic := '0';
   shared variable SVARIABLE5 : signed(CONSTANT1-1 downto 0) := x"CAFE0007";
   shared variable SVARIABLE6 : unsigned(15 downto 0) := (others => '1');
   --
   shared variable SVARIABLE7 : integer := CONSTANT1*8/4-2**(CONSTANT1-1);

   ----------------------------------------------------------------------------
   -- Ignored or Unsupported
   ----------------------------------------------------------------------------

   function function1 (x : integer) return natural;

   function function1 (x : integer) return natural is
   begin
   end function1;

   component COMPONENT1
      generic (WIDTH : integer);
      port (
         data_i : in std_logic_vector(WIDTH-1 downto 0);
         data_o : out std_logic
      );
   end component;

begin
end architecture Declarations_Arch;
