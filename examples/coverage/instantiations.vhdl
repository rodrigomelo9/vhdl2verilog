library IEEE;
use IEEE.std_logic_1164.all;

entity Instantiated is
   generic (
      AWIDTH, DWIDTH  : positive:=8;
      DEPTH           : natural:=0;
      OUTREG          : boolean :=FALSE
   );
   port (
      clk1_i, clk2_i, wen1_i :  in std_logic;
      addr1_i, addr2_i       :  in std_logic_vector(AWIDTH-1 downto 0);
      data1_i                :  in std_logic_vector(DWIDTH-1 downto 0);
      data2_o                : out std_logic_vector(DWIDTH-1 downto 0)
   );
end entity Instantiated;

--

library IEEE;
use IEEE.std_logic_1164.all;

entity Instantiations is
   generic (
      AWIDTH : positive :=8;
      DWIDTH : positive :=8;
      DEPTH  : natural  :=0;
      OUTREG : boolean  :=FALSE
   );
   port (
      clk1_i, clk2_i, clk3_i, clk4_i, clk5_i      :  in std_logic;
      wen1_i                                      :  in std_logic;
      addr1_i, addr2_i, addr3_i, addr4_i, addr5_i :  in std_logic_vector(AWIDTH-1 downto 0);
      data1_i                                     :  in std_logic_vector(DWIDTH-1 downto 0);
      data2_o, data3_o, data4_o, data5_o          : out std_logic_vector(DWIDTH-1 downto 0)
   );
end entity Instantiations;

architecture Instantiations_Arch of Instantiations is

   component Instantiated is
      generic (
         AWIDTH  : positive := 8;
         DWIDTH  : positive := 8;
         DEPTH   : natural  := 0;
         OUTREG  : boolean  := FALSE
      );
      port (
         clk1_i  :  in std_logic;
         clk2_i  :  in std_logic;
         wen1_i  :  in std_logic;
         addr1_i :  in std_logic_vector(AWIDTH-1 downto 0);
         addr2_i :  in std_logic_vector(AWIDTH-1 downto 0);
         data1_i :  in std_logic_vector(DWIDTH-1 downto 0);
         data2_o : out std_logic_vector(DWIDTH-1 downto 0)
      );
   end component Instantiated;

begin

   inst1 : Instantiated
   generic map (
      AWIDTH => AWIDTH, DWIDTH => DWIDTH, DEPTH => DEPTH, OUTREG => OUTREG
   )
   port map (
      clk1_i  => clk1_i,  clk2_i  => clk2_i,
      wen1_i  => wen1_i,
      addr1_i => addr1_i, addr2_i => addr2_i,
      data1_i => data1_i, data2_o => data2_o
   );

   inst2 : component Instantiated
   generic map (
      AWIDTH => AWIDTH, DWIDTH => DWIDTH, DEPTH => DEPTH, OUTREG => OUTREG
   )
   port map (
      clk1_i  => clk1_i,  clk2_i  => clk3_i,
      wen1_i  => wen1_i,
      addr1_i => addr1_i, addr2_i => addr3_i,
      data1_i => data1_i, data2_o => data3_o
   );

   inst3 : entity work.Instantiated
   generic map (
      AWIDTH => AWIDTH, DWIDTH => DWIDTH, DEPTH => DEPTH, OUTREG => OUTREG
   )
   port map (
      clk1_i  => clk1_i,  clk2_i  => clk4_i,
      wen1_i  => wen1_i,
      addr1_i => addr1_i, addr2_i => addr4_i,
      data1_i => data1_i, data2_o => data4_o
   );

   inst4 : Instantiated -- by position (not recommended)
   generic map (AWIDTH, DWIDTH, DEPTH, OUTREG)
   port map (clk1_i, clk5_i, wen1_i, addr1_i, addr5_i, data1_i, data5_o);

   inst5 : Instantiated
   port map (
      clk1_i  => '0',  clk2_i  => '1',
      wen1_i  => '1',
      addr1_i => "00110", addr2_i => (others => '0'),
      data1_i => (others => '1'), data2_o => open
   );

   inst6 : Instantiated
   port map ('0', '1', '1', "00110", (others => '0'), (others => '1'), open);

end architecture Instantiations_Arch;
