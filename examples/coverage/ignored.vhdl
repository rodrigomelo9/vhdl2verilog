library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Ignored is
   port (
      clk : in std_logic
   );
end entity Ignored;

architecture Ignored_Arch of Ignored is

   signal signal1, signal2 : signed(7 downto 0);
   signal signal3, signal4 : signed(7 downto 0);
   signal signal5, signal6 : signed(7 downto 0);
   signal signal_reg : signed(7 downto 0) register;
   signal signal_bus : signed(7 downto 0) bus;

begin

            postponed signal1 <= signal2;
   label1 :           signal3 <= signal4;
   label2 : postponed signal5 <= signal6;

   signal1 <= unaffected ;
   signal1 <= signal2 when clk = '1' else
              signal3 when clk = '0' else
              unaffected;

   signal3 <= reject 10 ns inertial signal4 after 10 ns;
   signal5 <= transport signal6 after 10 ns;

   aux : block (clk = '1')
      disconnect signal_reg : signed after 15 ns;
      disconnect others : signed after 20 ns;
      disconnect all : signed after 25 ns;
   begin
      signal_reg <= guarded signal1;
      label3 : signal_bus <= guarded signal2 after 5 ns;
   end block;

end architecture Ignored_Arch;
