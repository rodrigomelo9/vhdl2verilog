library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Operators is
end entity Operators;

architecture Operators_Arch of Operators is

   signal result   : signed(7 downto 0);
   signal op1, op2 : signed(7 downto 0);
   signal flag     : boolean;

begin

   result <= op1  and op2;
   result <= op1   or op2;
   result <= op1 nand op2;
   result <= op1  nor op2;
   result <= op1  xor op2;
   result <= op1 xnor op2;
   result <= not op1;

   flag <= (op1=1 and  op2/=2) or (op1<3 or op2<=4) or (op1>5 nand op2>=6) or (op1=7 nor op2=8) or (not (op1=9));

   result <= abs op1;
--   result <= op1 sll op2;
--   result <= op1 srl op2;
--   result <= op1 ror op2;
--   result <= op1 rol op2;
   result <= op1 rem op2;
   result <= op1 mod op2;

   result <= op1  + op2;
   result <= op1  - op2;
   result <= op1  * op2;
   result <= op1  / op2;
   result <= op1  & op2;
--   result <= op1 ** op2;

end architecture Operators_Arch;
