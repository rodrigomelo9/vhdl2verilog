/*
  vhdl2verilog, a compiler from synthesizable VHDL to Verilog
  Copyright (C) 2017-2019, Rodrigo A. Melo

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

%{
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include "support.h"

int count = 0;
int debug = 0;
int archs; // To store the number of ARCHITECTUREs per ENTITY
%}

%define api.value.type {char *}
%define parse.error verbose

// The 97 reserved words
%token RW_ABS            RW_ACCESS         RW_AFTER          RW_ALIAS
%token RW_ALL            RW_AND            RW_ARCHITECTURE   RW_ARRAY
%token RW_ASSERT         RW_ATTRIBUTE      RW_BEGIN          RW_BLOCK
%token RW_BODY           RW_BUFFER         RW_BUS            RW_CASE
%token RW_COMPONENT      RW_CONFIGURATION  RW_CONSTANT       RW_DISCONNECT
%token RW_DOWNTO         RW_ELSE           RW_ELSIF          RW_END
%token RW_ENTITY         RW_EXIT           RW_FILE           RW_FOR
%token RW_FUNCTION       RW_GENERATE       RW_GENERIC        RW_GROUP
%token RW_GUARDED        RW_IF             RW_IMPURE         RW_IN
%token RW_INERTIAL       RW_INOUT          RW_IS             RW_LABEL
%token RW_LIBRARY        RW_LINKAGE        RW_LITERAL        RW_LOOP
%token RW_MAP            RW_MOD            RW_NAND           RW_NEW
%token RW_NEXT           RW_NOR            RW_NOT            RW_NULL
%token RW_OF             RW_ON             RW_OPEN           RW_OR
%token RW_OTHERS         RW_OUT            RW_PACKAGE        RW_PORT
%token RW_POSTPONED      RW_PROCEDURE      RW_PROCESS        RW_PURE
%token RW_RANGE          RW_RECORD         RW_REGISTER       RW_REJECT
%token RW_REM            RW_REPORT         RW_RETURN         RW_ROL
%token RW_ROR            RW_SELECT         RW_SEVERITY       RW_SHARED
%token RW_SIGNAL         RW_SLA            RW_SLL            RW_SRA
%token RW_SRL            RW_SUBTYPE        RW_THEN           RW_TO
%token RW_TRANSPORT      RW_TYPE           RW_UNAFFECTED     RW_UNITS
%token RW_UNTIL          RW_USE            RW_VARIABLE       RW_WAIT
%token RW_WHEN           RW_WHILE          RW_WITH           RW_XNOR
%token RW_XOR

// Compound delimiters
%token ARROW "=>" VASSIGN ":=" LE "<=" GE ">=" BOX "<>" NE "/=" POW "**"

// Other lexical elements
%token IDENTIFIER ABSTRACT_LITERAL CHARACTER_LITERAL STRING_LITERAL BIT_STRING_LITERAL

%%

/******************************************************************************
 Design Units
******************************************************************************/

design_file :
       design_unit                 { print_debug("design_file", 1, 1); }
     | design_file design_unit     { print_debug("design_file", 2, 1); }

design_unit :
       context_clause library_unit {
              print_debug("design_unit", 1, 1);
              $$ = NULL;
              if ($2) {
                 printf("%s\n", $2);
                 free($2);
              }
       }

library_unit :
       primary_unit                { print_debug("library_unit", 1, 1); }
     | secondary_unit              { print_debug("library_unit", 2, 1); }

primary_unit :
       entity_declaration          { print_debug("primary_unit", 1, 1); }
     | configuration_declaration   { print_debug("primary_unit", 2, 1); $$ = NULL; }
     | package_declaration         { print_debug("primary_unit", 3, 1); $$ = NULL; }

secondary_unit :
       architecture_body           { print_debug("secondary_unit", 1, 1); }
     | package_body                { print_debug("secondary_unit", 2, 1); $$ = NULL; }

/******************************************************************************
 Library
******************************************************************************/

context_clause :
       %empty                      {
              print_debug("context_clause", 1, 1);
              $$ = NULL;
       }
     | context_clause context_item {
              print_debug("context_clause", 2, 1);
              $$ = NULL;
              multifree(2, $1, $2);
       }

context_item :
       library_clause { print_debug("context_item", 1, 1); }
     | use_clause     { print_debug("context_item", 2, 1); }

library_clause :
       RW_LIBRARY logical_name_list ';' {
              print_debug("library_clause", 1, 1);
              $$ = NULL;
              free($2);
       }

logical_name_list :
       IDENTIFIER { print_debug("logical_name_list", 1, 1); }
     | logical_name_list ',' IDENTIFIER { print_debug("logical_name_list", 2, 0); }

use_clause :
       RW_USE selected_name opts_colon_selected_name ';' {
              print_debug("use_clause", 1, 1);
              $$ = NULL;
              multifree(2, $2, $3);
       }

selected_name :
       prefix '.' suffix {
              print_debug("selected_name", 1, 1);
              $$ = getstr(1, $3);
              //$$ = getstr(3, $1, ".", $3);
              free($1);
              //free($3); // Segmentation fault
       }

opts_colon_selected_name :
       %empty { print_debug("opts_colon_selected_name", 1, 1); $$ = NULL; }
     | opts_colon_selected_name ',' selected_name {
              print_debug("opts_colon_selected_name", 2, 1);
              $$ = NULL;
              multifree(2, $1, $3);
       }

/******************************************************************************
 Entity
******************************************************************************/

entity_declaration :
       RW_ENTITY IDENTIFIER RW_IS entity_header entity_declarative_part opt_begin_entity_statement_part RW_END opt_entity opt_identifier ';' {
              print_debug("entity_declaration", 1, 1);
              $$ = getstr(4, "module ", $2, $4, "\n");
              multifree(4, $2, $4, $5, $6);
              archs = 0;
       }

entity_header :
       opt_generic_clause opt_port_clause {
              print_debug("entity_header", 1, 1);
              $$ = getstr(3, $1, $2, ";");
              multifree(2, $1, $2);
       }

opt_generic_clause :
       %empty             { print_debug("opt_generic_clause", 1, 1); $$ = NULL; }
     | generic_clause     { print_debug("opt_generic_clause", 2, 1); }

opt_generic_map_aspect :
       %empty             { print_debug("opt_generic_map_aspect", 1, 1); $$ = NULL; }
     | generic_map_aspect { print_debug("opt_generic_map_aspect", 2, 1); }

opt_port_clause :
       %empty             { print_debug("opt_port_clause", 1, 1); $$ = NULL; }
     | port_clause        { print_debug("opt_port_clause", 2, 1); }

opt_port_map_aspect :
       %empty             { print_debug("opt_port_map_aspect", 1, 1); $$ = NULL; }
     | port_map_aspect    { print_debug("opt_port_map_aspect", 2, 1); }

generic_clause :
       RW_GENERIC '(' generic_list ')' ';' {
              print_debug("generic_clause", 1, 1);
              $$ = getstr(3, " #(\n", $3, "\n)");
              free($3);
       }

generic_list :
       generic_element { print_debug("generic_list", 1, 1); }
     | generic_list ';' generic_element {
              print_debug("generic_list", 2, 1);
              $$ = getstr(3, $1, ",\n", $3);
              multifree(2, $1, $3);
       }

generic_element :
       opt_constant identifier_list ':' opt_in subtype_indication opt_assign_exp {
                print_debug("generic_element", 1, 1);
                $$ = getstr(5, "   ", "parameter ", $5, $2, ($6) ? $6 : getstr(1, " = 0") );
                multifree(3, $2, $5, $6);
       }

port_clause :
       RW_PORT '(' port_list ')' ';' {
              print_debug("port_clause", 1, 1);
              $$ = getstr(3, " (\n", $3, "\n)");
              free($3);
       }

port_list :
       port_element { print_debug("port_list", 1, 1); }
     | port_list ';' port_element {
              print_debug("port_list", 2, 1);
              $$ = getstr(3, $1, ",\n", $3);
              multifree(2, $1, $3);
       }

port_element :
       opt_signal identifier_list ':' mode subtype_indication opt_bus opt_assign_exp {
                print_debug("port_element", 1, 1);
                $$ = getstr(6, "   ", $4, " ", $5, $2, $7);
                multifree(4, $2, $4, $5, $7);
       }

entity_declarative_part :
       %empty { print_debug("entity_declarative_part", 1, 1); $$ = NULL; }
     | entity_declarative_part entity_declarative_item {
              print_debug("entity_declarative_part", 2, 0);
              $$ = NULL;
              multifree(2, $1, $2);
              print_support("the 'entity declarative part' is unsupported, ignored.", WARMSG);
       }

entity_declarative_item :
       subprogram_declaration      { print_debug("entity_declarative_item", 1, 0);  }
     | subprogram_body             { print_debug("entity_declarative_item", 2, 0);  }
     | type_declaration            { print_debug("entity_declarative_item", 3, 0);  }
     | subtype_declaration         { print_debug("entity_declarative_item", 4, 0);  }
     | constant_declaration        { print_debug("entity_declarative_item", 5, 0);  }
     | signal_declaration          { print_debug("entity_declarative_item", 6, 0);  }
     | variable_declaration        { print_debug("entity_declarative_item", 7, 0);  }
     | file_declaration            { print_debug("entity_declarative_item", 8, 0);  }
     | alias_declaration           { print_debug("entity_declarative_item", 9, 0);  }
     | attribute_declaration       { print_debug("entity_declarative_item", 10, 0); }
     | attribute_specification     { print_debug("entity_declarative_item", 11, 0); }
     | disconnection_specification { print_debug("entity_declarative_item", 12, 0); }
     | use_clause                  { print_debug("entity_declarative_item", 13, 0); }
     | group_template_declaration  { print_debug("entity_declarative_item", 14, 0); }
     | group_declaration           { print_debug("entity_declarative_item", 15, 0); }

entity_statement_part :
       %empty { print_debug("entity_statement_part", 1, 0); $$ = NULL; }
     | entity_statement_part entity_statement {
              print_debug("entity_statement_part", 2, 0);
              $$ = NULL;
              multifree(2, $1, $2);
              print_support("the 'entity statement part' is unsupported, ignored.", WARMSG);
       }

entity_statement :
       concurrent_assertion_statement      { print_debug("entity_statement", 1, 0); }
     | concurrent_procedure_call_statement { print_debug("entity_statement", 2, 0); }
     | process_statement                   { print_debug("entity_statement", 3, 0); }

/******************************************************************************
 Architecture
******************************************************************************/

architecture_body :
       RW_ARCHITECTURE IDENTIFIER RW_OF name RW_IS architecture_declarative_part RW_BEGIN opts_concurrent_statement RW_END opt_arch opt_identifier ';' {
              print_debug("architecture_body", 1, 1);
              if (archs == 0) {
                 $$ = getstr(4, $6, "\n", $8, "endmodule");
              } else {
                 $$ = NULL;
                 print_support("more than one 'architecture' per 'entity' is not yet supported, ignored.", WARMSG);
              }
              multifree(4, $2, $4, $6, $8);
              archs++;
       }

architecture_declarative_part :
       opts_block_declarative_item { print_debug("architecture_declarative_part", 1, 1); }

opts_concurrent_statement :
       %empty { print_debug("opts_concurrent_statement", 1, 1); $$ = NULL; }
     | opts_concurrent_statement concurrent_statement {
              print_debug("opts_concurrent_statement", 2, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

opts_block_declarative_item :
       %empty { print_debug("opts_block_declarative_item", 1, 1); $$ = NULL; }
     | opts_block_declarative_item block_declarative_item {
              print_debug("opts_block_declarative_item", 2, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

block_declarative_item :
       subprogram_declaration      { print_debug("block_declarative_item", 1, 1);  }
     | subprogram_body             { print_debug("block_declarative_item", 2, 1);  }
     | type_declaration            { print_debug("block_declarative_item", 3, 1);  }
     | subtype_declaration         { print_debug("block_declarative_item", 4, 1);  }
     | constant_declaration        { print_debug("block_declarative_item", 5, 1);  }
     | signal_declaration          { print_debug("block_declarative_item", 6, 1);  }
     | variable_declaration        { print_debug("block_declarative_item", 7, 1);  }
     | file_declaration            { print_debug("block_declarative_item", 8, 0);  }
     | alias_declaration           { print_debug("block_declarative_item", 9, 0);  }
     | component_declaration       { print_debug("block_declarative_item", 10, 1); }
     | attribute_declaration       { print_debug("block_declarative_item", 11, 0); }
     | attribute_specification     { print_debug("block_declarative_item", 12, 0); }
     | configuration_specification { print_debug("block_declarative_item", 13, 0); }
     | disconnection_specification { print_debug("block_declarative_item", 14, 1); }
     | use_clause                  { print_debug("block_declarative_item", 15, 0); }
     | group_template_declaration  { print_debug("block_declarative_item", 16, 1); }
     | group_declaration           { print_debug("block_declarative_item", 17, 0); }

/******************************************************************************
 Package
******************************************************************************/

package_declaration :
       RW_PACKAGE IDENTIFIER RW_IS package_declarative_part RW_END opt_package opt_identifier ';' {
              print_debug("package_declaration", 1, 1);
              $$ = NULL;
              free($2);
              print_support("'packages' are not yet supported, ignored.", WARMSG);
       }

package_declarative_part :
       %empty { print_debug("package_declarative_part", 1, 1); $$ = NULL; }
     | package_declarative_part package_declarative_item {
              print_debug("package_declarative_part", 2, 0);
       }

package_declarative_item :
       subprogram_declaration      { print_debug("package_declarative_item", 1, 0);  }
     | type_declaration            { print_debug("package_declarative_item", 2, 0);  }
     | subtype_declaration         { print_debug("package_declarative_item", 3, 0);  }
     | constant_declaration        { print_debug("package_declarative_item", 4, 0);  }
     | signal_declaration          { print_debug("package_declarative_item", 5, 0);  }
     | variable_declaration        { print_debug("package_declarative_item", 6, 0);  }
     | file_declaration            { print_debug("package_declarative_item", 7, 0);  }
     | alias_declaration           { print_debug("package_declarative_item", 8, 0);  }
     | component_declaration       { print_debug("package_declarative_item", 9, 0);  }
     | attribute_declaration       { print_debug("package_declarative_item", 10, 0); }
     | attribute_specification     { print_debug("package_declarative_item", 11, 0); }
     | disconnection_specification { print_debug("package_declarative_item", 12, 0); }
     | use_clause                  { print_debug("package_declarative_item", 13, 0); }
     | group_template_declaration  { print_debug("package_declarative_item", 14, 0); }
     | group_declaration           { print_debug("package_declarative_item", 15, 0); }

package_body :
       RW_PACKAGE RW_BODY IDENTIFIER RW_IS package_body_declarative_part RW_END opt_package_body opt_identifier ';' {
              print_debug("package_body", 1, 1);
              $$ = NULL;
              free($3);
              print_support("'package bodys' are not yet supported, ignored.", WARMSG);
       }

package_body_declarative_part :
       %empty { print_debug("package_body_declarative_part", 1, 1); $$ = NULL; }
     | package_body_declarative_part package_body_declarative_item {
              print_debug("package_body_declarative_part", 2, 0);
       }

package_body_declarative_item :
       subprogram_declaration     { print_debug("package_body_declarative_item", 1, 0);  }
     | subprogram_body            { print_debug("package_body_declarative_item", 2, 0);  }
     | type_declaration           { print_debug("package_body_declarative_item", 3, 0);  }
     | subtype_declaration        { print_debug("package_body_declarative_item", 4, 0);  }
     | constant_declaration       { print_debug("package_body_declarative_item", 5, 0);  }
     | variable_declaration       { print_debug("package_body_declarative_item", 6, 0);  }
     | file_declaration           { print_debug("package_body_declarative_item", 7, 0);  }
     | alias_declaration          { print_debug("package_body_declarative_item", 8, 0);  }
     | use_clause                 { print_debug("package_body_declarative_item", 9, 0);  }
     | group_template_declaration { print_debug("package_body_declarative_item", 10, 0); }
     | group_declaration          { print_debug("package_body_declarative_item", 11, 0); }

/******************************************************************************
 Configuration
******************************************************************************/

configuration_declaration :
       RW_CONFIGURATION IDENTIFIER RW_OF name RW_IS configuration_declarative_part block_configuration RW_END opt_config opt_identifier ';' {
              print_debug("configuration_declaration", 1, 1);
              $$ = NULL;
              multifree(4, $2, $4, $6, $7);
              print_support("'configuration declarations' are unsupported, ignored.", WARMSG);
       }

configuration_declarative_part :
       %empty { print_debug("configuration_declarative_part", 1, 1); $$ = NULL; }
     | configuration_declarative_part configuration_declarative_item {
              print_debug("configuration_declarative_part", 2, 1);
              $$ = NULL;
              multifree(2, $1, $2);
       }

configuration_declarative_item :
       use_clause {
              print_debug("configuration_declarative_item", 1, 1);
              $$ = NULL;
              free($1);
       }
     | attribute_specification {
              print_debug("configuration_declarative_item", 2, 0);
       }
     | group_declaration {
              print_debug("configuration_declarative_item", 3, 0);
       }

block_configuration :
       RW_FOR block_specification opts_use_clause opts_configuration_item RW_END RW_FOR ';' {
              print_debug("block_configuration", 1, 1);
              $$ = NULL;
              multifree(3 ,$2, $3, $4);
       }

block_specification :
       name {
              print_debug("block_specification", 1, 1);
              $$ = NULL;
              free($1);
       }
     | IDENTIFIER '(' index_specification ')' {
              print_debug("block_specification", 2, 0);
       }

index_specification :
       discrete_range {
              print_debug("index_specification", 1, 0);
       }
     | expression {
              print_debug("index_specification", 2, 0);
       }

configuration_item :
       block_configuration {
              print_debug("configuration_item", 1, 0);
       }
     | component_configuration {
              print_debug("configuration_item", 2, 1);
              $$ = NULL;
              free($1);
       }

component_configuration :
       RW_FOR component_specification opt_binding_indication opt_block_config RW_END RW_FOR ';' {
              print_debug("component_configuration", 1, 1);
              $$ = NULL;
              multifree(3, $2, $3, $4);
       }

/******************************************************************************
 Sub-program
******************************************************************************/

subprogram_declaration :
       subprogram_specification ';' {
              print_debug("subprogram_declaration", 1, 1);
              $$ = NULL;
              free($1);
       }

subprogram_specification :
       RW_PROCEDURE designator opt_subprogram_list {
              print_debug("subprogram_specification", 1, 0);
       }
     | opt_pure_or_impure RW_FUNCTION designator opt_subprogram_list RW_RETURN name {
              print_debug("subprogram_specification", 2, 1);
              $$ = NULL;
              multifree(2, $3, $4);
              print_support("'functions' are not yet supported, ignored.", WARMSG);
       }

designator :
       IDENTIFIER     { print_debug("designator", 1, 1); }
     | STRING_LITERAL { print_debug("designator", 2, 0); }

opt_subprogram_list :
       %empty                  { print_debug("opt_subprogram_list", 1, 0); $$ = NULL; }
     | '(' subprogram_list ')' {
                print_debug("opt_subprogram_list", 2, 1);
                $$ = getstr(3, "(", $2, ")");
                free($2);
       }

subprogram_list :
       subprogram_element                     { print_debug("subprogram_list", 1, 1); }
     | subprogram_list ';' subprogram_element { print_debug("subprogram_list", 2, 0); }

subprogram_element :
       opt_constant identifier_list ':' opt_in subtype_indication opt_assign_exp {
                print_debug("subprogram_element", 1, 0);
       }
     | opt_signal identifier_list ':' mode subtype_indication opt_bus opt_assign_exp {
                print_debug("subprogram_element", 2, 0);
       }
     | identifier_list ':' mode subtype_indication opt_assign_exp {
                print_debug("subprogram_element", 3, 1);
                $$ = NULL;
                multifree(3, $1, $3, $4);
       }
     | RW_VARIABLE identifier_list ':' mode subtype_indication opt_assign_exp {
                print_debug("subprogram_element", 4, 0);
       }
     | RW_FILE identifier_list subtype_indication {
                print_debug("subprogram_element", 5, 0);
       }

subprogram_body :
       subprogram_specification RW_IS subprogram_declarative_part RW_BEGIN opts_sequential_statement RW_END opt_subprogram_kind opt_designator ';' {
              print_debug("subprogram_body", 1, 1);
              $$ = NULL;
              multifree(5, $1, $3, $5, $7, $8);
       }

subprogram_declarative_part :
       %empty { print_debug("subprogram_declarative_part", 1, 1); $$ = NULL; }
     | subprogram_declarative_part subprogram_declarative_item {
              print_debug("subprogram_declarative_part", 2, 0);
       }

subprogram_declarative_item :
       subprogram_declaration     { print_debug("subprogram_declarative_item", 1, 0); }
     | subprogram_body            { print_debug("subprogram_declarative_item", 2, 0); }
     | type_declaration           { print_debug("subprogram_declarative_item", 3, 0); }
     | subtype_declaration        { print_debug("subprogram_declarative_item", 4, 0); }
     | constant_declaration       { print_debug("subprogram_declarative_item", 5, 0); }
     | variable_declaration       { print_debug("subprogram_declarative_item", 6, 0); }
     | file_declaration           { print_debug("subprogram_declarative_item", 7, 0); }
     | alias_declaration          { print_debug("subprogram_declarative_item", 8, 0); }
     | attribute_declaration      { print_debug("subprogram_declarative_item", 9, 0); }
     | attribute_specification    { print_debug("subprogram_declarative_item", 10, 0); }
     | use_clause                 { print_debug("subprogram_declarative_item", 11, 0); }
     | group_template_declaration { print_debug("subprogram_declarative_item", 12, 0); }
     | group_declaration          { print_debug("subprogram_declarative_item", 13, 0); }

subprogram_kind :
       RW_PROCEDURE {
              print_debug("subprogram_kind", 1, 0);
       }
     | RW_FUNCTION {
              print_debug("subprogram_kind", 2, 0);
       }

opt_designator :
       %empty     { print_debug("opt_designator", 1, 0); $$ = NULL; }
     | designator { print_debug("opt_designator", 2, 1); }

/******************************************************************************
 Optionals
******************************************************************************/

opt_abstract_literal :
       %empty             { print_debug("opt_abstract_literal", 1, 0); $$ = NULL; }
     | ABSTRACT_LITERAL   { print_debug("opt_abstract_literal", 2, 1); }

opt_arch :
       %empty             { print_debug("opt_arch", 1, 1); $$ = NULL; }
     | RW_ARCHITECTURE    { print_debug("opt_arch", 2, 1); }

opt_bus :
       %empty             { print_debug("opt_bus", 1, 1); $$ = NULL; }
     | RW_BUS             {
                print_debug("opt_bus", 2, 0);
                $$ = NULL;
                print_support("'bus' keyword is ignored.", INFMSG);
       }

opt_config :
       %empty             { print_debug("opt_config", 1, 1); $$ = NULL; }
     | RW_CONFIGURATION   { print_debug("opt_config", 2, 1); }

opt_constant :            %empty { $$ = NULL; } | RW_CONSTANT

opt_entity :
       %empty             { print_debug("opt_entity", 1, 1); $$ = NULL; }
     | RW_ENTITY          { print_debug("opt_entity", 2, 1); }

opt_guarded :
       %empty             { print_debug("opt_guarded", 1, 1); $$ = NULL; }
     | RW_GUARDED         {
              print_debug("opt_guarded", 2, 1);
              $$ = NULL;
              print_support("'guarded' keyword is ignored.", INFMSG);
       }

opt_in :
       %empty             { print_debug("opt_in", 1, 1); $$ = NULL; }
     | RW_IN              { print_debug("opt_in", 2, 1); }

opt_is :
       %empty             { print_debug("opt_is", 1, 1); $$ = NULL; }
     | RW_IS              { print_debug("opt_is", 2, 1); }

opt_package :
       %empty             { print_debug("opt_package", 1, 1); $$ = NULL;}
     | RW_PACKAGE         { print_debug("opt_package", 2, 1); }

opt_package_body :
       %empty             { print_debug("opt_package_body", 1, 1); $$ = NULL; }
     | RW_PACKAGE RW_BODY { print_debug("opt_package_body", 2, 1); }

opt_postponed :
       %empty             { print_debug("opt_postponed", 1, 1); $$ = NULL; }
     | RW_POSTPONED       {
              print_debug("opt_postponed", 2, 0);
              print_support("'postponed' keyword is ignored.", INFMSG);
       }

opt_pure_or_impure :
       %empty             { print_debug("opt_pure_or_impure", 1, 1); $$ = NULL; }
     | RW_PURE            { print_debug("opt_pure_or_impure", 2, 0); }
     | RW_IMPURE          { print_debug("opt_pure_or_impure", 3, 0); }

opt_shared :
       %empty             { print_debug("opt_shared", 1, 0); $$ = NULL; }
     | RW_SHARED          { print_debug("opt_shared", 2, 1); }

opt_signal :              %empty { $$ = NULL; } | RW_SIGNAL

//////////////////////////////////////////////////////////////////////////////

scalar_type_definition :
       enumeration_type_definition { print_debug("scalar_type_definition", 1, 1); }
     | physical_type_definition    { print_debug("scalar_type_definition", 2, 1); }
     | range_constraint {
              print_debug("scalar_type_definition", 3, 1);
              print_support("user defined integer data types are unsupported.", WARMSG);
       }

enumeration_type_definition :
       '(' enumeration_literal opts_colon_enumeration_literal ')' {
              char *auxstr;
              print_debug("enumeration_type_definition", 1, 1);
              auxstr = itoa(0);
              $$ = getstr(6, "   localparam\n      ", $2, " = ", auxstr, $3, ";\n\n");
              multifree(3, $2, $3, auxstr);
       }

enumeration_literal :
       IDENTIFIER        { print_debug("enumeration_literal", 1, 1); }
     | CHARACTER_LITERAL { print_debug("enumeration_literal", 2, 1); }

opts_colon_enumeration_literal :
       %empty { print_debug("opts_colon_enumeration_literal", 1, 1); $$ = NULL; }
     | opts_colon_enumeration_literal ',' enumeration_literal {
              char *auxstr;
              print_debug("opts_colon_enumeration_literal", 2, 1);
              auxstr = itoa(++count);
              $$ = getstr(5, $1, ",\n      ", $3, " = ", auxstr);
              multifree(3, $1, $3, auxstr);
              // TODO
              //$$ = getstr(3, $1, ", ", $2);
              //multifree(2, $1, $3);
       }

range_constraint :
       RW_RANGE range {
              print_debug("range_constraint", 1, 1);
              $$ = NULL;
              free($2);
       }

range :
       attribute_name {
              print_debug("range", 1, 0);
       }
     | simple_expression direction simple_expression {
              print_debug("range", 2, 1);
              $$ = getstr(3, $1, $2, $3);
              multifree(3, $1, $2, $3);
       }

direction :
       RW_TO     { print_debug("direction", 1, 1); $$ = getstr(1, ":"); }
     | RW_DOWNTO { print_debug("direction", 2, 1); $$ = getstr(1, ":"); }

physical_type_definition :
       range_constraint RW_UNITS primary_unit_declaration opts_secondary_unit_declaration RW_END RW_UNITS opt_identifier {
              print_debug("physical_type_definition", 1, 1);
              $$ = getstr(3, $3, $4, "\n");
              multifree(4, $1, $3, $4, $7);
       }

primary_unit_declaration :
       IDENTIFIER ';' {
              print_debug("primary_unit_declaration", 1, 1);
              $$ = getstr(3,"`define ", $1, " ( * 1);\n");
              free($1);
       }

opts_secondary_unit_declaration :
       %empty { print_debug("opts_secondary_unit_declaration", 1, 1); $$ = NULL; }
     | opts_secondary_unit_declaration secondary_unit_declaration {
              print_debug("opts_secondary_unit_declaration", 2, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

secondary_unit_declaration :
       IDENTIFIER '=' opt_abstract_literal name ';' {
              print_debug("secondary_unit_declaration", 1, 1);
              $$ = getstr(7,"`define ", $1, " ( * ", $3, " * `",$4, ");\n");
              multifree(2, $1, $3);
       }

composite_type_definition :
       array_type_definition {
              print_debug("composite_type_definition", 1, 0);
       }
     | record_type_definition {
              print_debug("composite_type_definition", 2, 1);
              print_support("record data types are unsupported.", WARMSG);
       }

array_type_definition :
       unconstrained_array_definition {
              print_debug("array_type_definition", 1, 0);
       }
     | constrained_array_definition {
              print_debug("array_type_definition", 2, 0);
       }

unconstrained_array_definition :
       RW_ARRAY '(' index_subtype_definition opts_colon_index_subtype_def ')' RW_OF subtype_indication {
              print_debug("unconstrained_array_definition", 1, 0);
       }

constrained_array_definition :
       RW_ARRAY index_constraint RW_OF subtype_indication {
              print_debug("constrained_array_definition", 1, 0);
       }

index_subtype_definition :
       name RW_RANGE "<>" {
              print_debug("index_subtype_definition", 1, 0);
       }

index_constraint :
       '(' discrete_range opts_colon_discrete_range ')' {
              print_debug("index_constraint", 1, 1);
              $$ = getstr(3, "[", $2, "] ");
              multifree(2, $2, $3);
       }

discrete_range :
       subtype_indication {
              print_debug("discrete_range", 1, 0);
       }
     | range { print_debug("discrete_range", 2, 1); }

opts_colon_discrete_range :
       %empty { print_debug("opts_colon_discrete_range", 1, 1); $$ = NULL; }
     | opts_colon_discrete_range ',' discrete_range {
              print_debug("opts_colon_discrete_range", 2, 0);
       }

record_type_definition :
       RW_RECORD element_declaration opts_element_declaration RW_END RW_RECORD opt_identifier {
              print_debug("record_type_definition", 1, 1);
              $$ = NULL;
       }

element_declaration :
       identifier_list ':' subtype_indication ';' {
              print_debug("element_declaration", 1, 1);
              $$ = NULL;
              multifree(2, $1, $3);
       }

opts_element_declaration :
       %empty { print_debug("opts_element_declaration", 1, 1); $$ = NULL; }
     | opts_element_declaration element_declaration {
              print_debug("opts_element_declaration", 2, 1);
              $$ = NULL;
              multifree(2, $1, $2);
       }

identifier_list :
       IDENTIFIER { print_debug("identifier_list", 1, 1); }
     | identifier_list ',' IDENTIFIER {
              print_debug("identifier_list", 2, 1);
              $$ = getstr(3, $1, ", ", $3);
              multifree(2, $1, $3);
       }

access_type_definition :
       RW_ACCESS subtype_indication {
              print_debug("access_type_definition", 1, 0);
       }

incomplete_type_declaration :
       RW_TYPE IDENTIFIER ';' {
              print_debug("incomplete_type_declaration", 1, 1);
              $$ = NULL;
              free($2);
              print_support("the 'incomplete type declarations' are ignored.", NOTMSG);
       }

file_type_definition :
       RW_FILE RW_OF name {
              print_debug("file_type_definition", 1, 0);
       }

type_declaration :
       full_type_declaration       { print_debug("type_declaration", 1, 1); }
     | incomplete_type_declaration { print_debug("type_declaration", 2, 1); }

full_type_declaration :
       RW_TYPE IDENTIFIER RW_IS type_definition ';' {
              print_debug("full_type_declaration", 1, 1);
              // TODO: define the TYPE here
              //$$ = getstr(3, "`define ", $2, $4);
              $$ = $4;
              free($2);
              count = 0;
       }

type_definition :
       scalar_type_definition    { print_debug("type_definition", 1, 1); }
     | composite_type_definition { print_debug("type_definition", 2, 1); }
     | access_type_definition    { print_debug("type_definition", 3, 0); }
     | file_type_definition      { print_debug("type_definition", 4, 0); }

subtype_declaration :
       RW_SUBTYPE IDENTIFIER RW_IS subtype_indication ';' {
              print_debug("subtype_declaration", 1, 1);
              $$ = getstr(5, "`define ", $2, " ", $4, "\n");
              multifree(2, $2, $4);
       }

subtype_indication :
       name opt_constraint {
              print_debug("subtype_indication", 1, 1);
              $$ = convtype($1, $2);
              multifree(2, $1, $2);
        }
     | name name opt_constraint {
              print_debug("subtype_indication", 2, 0);
       }

opt_constraint :
       %empty     { print_debug("opt_constraint", 1, 1); $$ = NULL; }
     | constraint { print_debug("opt_constraint", 2, 1); }

constraint :
       range_constraint { print_debug("constraint", 1, 0); }
     | index_constraint { print_debug("constraint", 2, 1); }

constant_declaration :
       RW_CONSTANT identifier_list ':' subtype_indication opt_assign_exp ';' {
              print_debug("constant_declaration", 1, 1);
              $$ = getstr(5, "   localparam ", $4, $2, $5, ";\n");
              multifree(3, $2, $4, $5);
       }

signal_declaration :
       RW_SIGNAL identifier_list ':' subtype_indication opt_signal_kind opt_assign_exp ';' {
              print_debug("signal_declaration", 1, 1);
              $$ = getstr(5, "   wire ", $4, $2, $6, ";\n");
              multifree(3, $2, $4, $6);
       }

opt_signal_kind :
       %empty { print_debug("opt_signal_kind", 1, 1); $$ = NULL; }
     | RW_REGISTER {
              print_debug("opt_signal_kind", 2, 1);
              $$ = NULL;
              print_support("'register' keyword is ignored.", INFMSG);
       }
     | RW_BUS {
              print_debug("opt_signal_kind", 3, 1);
              $$ = NULL;
              print_support("'bus' keyword is ignored.", INFMSG);
       }

variable_declaration :
       opt_shared RW_VARIABLE identifier_list ':' subtype_indication opt_assign_exp ';' {
              print_debug("variable_declaration", 1, 1);
              $$ = getstr(5, "   wire ", $5, $3, $6, ";\n");
              multifree(3, $3, $5, $6);
       }

file_declaration :
       RW_FILE identifier_list ':' subtype_indication opt_file_open_information ';' {
              print_debug("file_declaration", 1, 0);
       }

file_open_information :
       opt_open_expression RW_IS file_logical_name {
              print_debug("file_open_information", 1, 0);
       }

file_logical_name :
       expression {
              print_debug("file_logical_name", 1, 0);
       }

mode :
       RW_IN      {
                print_debug("mode", 1, 1);
                $$ = getstr(1, "input ");
       }
     | RW_OUT     {
                print_debug("mode", 2, 1);
                $$ = getstr(1, "output");
       }
     | RW_INOUT   {
                print_debug("mode", 3, 1);
                $$ = getstr(1, "inout ");
       }
     | RW_BUFFER  {
                print_debug("mode", 4, 1);
                $$ = getstr(1, "inout ");
                print_support("'buffer' is compiled as 'inout'.", NOTMSG);
       }
     | RW_LINKAGE {
                print_debug("mode", 5, 1);
                $$ = getstr(1, "inout ");
                print_support("'linkage' is compiled as 'inout'.", NOTMSG);
       }
     | %empty     {
                print_debug("mode", 6, 1);
                $$ = getstr(1, "input ");
       }

association_list :
       association_element { print_debug("association_list", 1, 1); }
     | association_list ',' association_element {
              print_debug("association_list", 2, 1);
              $$ = getstr(3, $1, ",\n", $3);
              multifree(2, $1, $3);
       }

association_element :
       opt_formal_part_arrow actual_part {
              print_debug("association_element", 1, 1);
              if ($1)
                 $$ = getstr(6, "      ", ".", $1 , " ( ", $2, " )");
              else
                 $$ = getstr(2, "      ", $2);
              multifree(2, $1, $2);
       }
       // option added by RAM to allow instantiation per position
     | name {
              print_debug("association_element", 2, 1);
              $$ = getstr(2, "      ", $1);
              free($1);
       }

formal_part :
       formal_designator { print_debug("formal_part", 1, 1); }
     | name '(' formal_designator ')' {
              print_debug("formal_part", 2, 0);
       }

formal_designator : name { print_debug("formal_designator", 1, 1); }

actual_part :
       actual_designator { print_debug("actual_part", 1, 1); }
     | name '(' actual_designator ')' {
              print_debug("actual_part", 2, 0);
       }

actual_designator :
       expression { print_debug("actual_designator", 1, 1); }
     | name       { print_debug("actual_designator", 2, 1); }
     | RW_OPEN    { print_debug("actual_designator", 3, 1); $$ = getstr(1, "/* open */"); }

alias_declaration :
       RW_ALIAS alias_designator opt_subtype_indication RW_IS name opt_signature ';' {
              print_debug("alias_declaration", 1, 0);
       }

alias_designator :
       IDENTIFIER        { print_debug("alias_designator", 1, 0); }
     | CHARACTER_LITERAL { print_debug("alias_designator", 2, 0); }
     | STRING_LITERAL    { print_debug("alias_designator", 3, 0); }

attribute_declaration :
       RW_ATTRIBUTE IDENTIFIER ':' name ';' {
              print_debug("attribute_declaration", 1, 0);
       }

component_declaration :
       RW_COMPONENT IDENTIFIER opt_is opt_generic_clause opt_port_clause RW_END RW_COMPONENT opt_identifier ';' {
              print_debug("component_declaration", 1, 1);
              $$ = NULL;
              multifree(4, $2, $4, $5, $8);
       }

group_template_declaration :
       RW_GROUP IDENTIFIER RW_IS '(' entity_class_entry_list ')' ';' {
              print_debug("group_template_declaration", 1, 1);
              $$ = NULL;
              multifree(2, $2, $5);
              print_support("the 'group template declaration' is unsupported, ignored.", WARMSG);
       }

entity_class_entry_list :
       entity_class_entry { print_debug("entity_class_entry_list", 1, 1); $$ = NULL; }
     | entity_class_entry_list ',' entity_class_entry { print_debug("entity_class_entry_list", 2, 1); $$ = NULL; }

entity_class_entry :
       entity_class opt_minor_mayor { print_debug("entity_class_entry", 1, 1); $$ = NULL; }

opt_minor_mayor :
       %empty { print_debug("opt_minor_mayor", 1, 1); $$ = NULL; }
     | "<>"   { print_debug("opt_minor_mayor", 2, 1); $$ = NULL; }

group_declaration :
       RW_GROUP IDENTIFIER ':' name '(' group_constituent_list ')' ';' {
              print_debug("group_declaration", 1, 0);
       }

group_constituent_list :
       group_constituent {
              print_debug("group_constituent_list", 1, 0);
       }
     | group_constituent_list ',' group_constituent {
              print_debug("group_constituent_list", 2, 0);
       }

group_constituent :
       name {
              print_debug("group_constituent", 1, 0);
       }
     | CHARACTER_LITERAL {
              print_debug("group_constituent", 2, 0);
       }

attribute_specification :
       RW_ATTRIBUTE IDENTIFIER RW_OF entity_specification RW_IS expression ';' {
              print_debug("attribute_specification", 1, 0);
       }

entity_specification :
       entity_name_list ':' entity_class {
              print_debug("entity_specification", 1, 0);
       }

entity_class :
       RW_ENTITY        { print_debug("entity_class", 1, 0); }
     | RW_ARCHITECTURE  { print_debug("entity_class", 2, 0); }
     | RW_CONFIGURATION { print_debug("entity_class", 3, 0); }
     | RW_PROCEDURE     { print_debug("entity_class", 4, 0); }
     | RW_FUNCTION      { print_debug("entity_class", 5, 0); }
     | RW_PACKAGE       { print_debug("entity_class", 6, 0); }
     | RW_TYPE          { print_debug("entity_class", 7, 0); }
     | RW_SUBTYPE       { print_debug("entity_class", 8, 0); }
     | RW_CONSTANT      { print_debug("entity_class", 9, 0); }
     | RW_SIGNAL        { print_debug("entity_class", 10, 1); }
     | RW_VARIABLE      { print_debug("entity_class", 11, 0); }
     | RW_COMPONENT     { print_debug("entity_class", 12, 0); }
     | RW_LABEL         { print_debug("entity_class", 13, 1); }
     | RW_LITERAL       { print_debug("entity_class", 14, 0); }
     | RW_UNITS         { print_debug("entity_class", 15, 0); }
     | RW_GROUP         { print_debug("entity_class", 16, 1); }
     | RW_FILE          { print_debug("entity_class", 17, 0); }

entity_name_list :
       entity_designator opts_entity_designator {
              print_debug("entity_name_list", 1, 0);
       }
     | RW_OTHERS {
              print_debug("entity_name_list", 2, 0);
       }
     | RW_ALL {
              print_debug("entity_name_list", 3, 0);
       }

opts_entity_designator :
       %empty { print_debug("opts_entity_designator", 1, 0); $$ = NULL; }
     | opts_entity_designator ',' entity_designator {
              print_debug("opts_entity_designator", 2, 0);
       }

entity_designator :
       entity_tag opt_signature {
              print_debug("entity_designator", 1, 0);
       }

entity_tag :
       IDENTIFIER {
              print_debug("entity_tag", 1, 0);
       }
     | CHARACTER_LITERAL {
              print_debug("entity_tag", 2, 0);
       }
     | STRING_LITERAL {
              print_debug("entity_tag", 3, 0);
       }

configuration_specification :
       RW_FOR component_specification binding_indication ';' {
              print_debug("configuration_specification", 1, 0);
       }

component_specification :
       instantiation_list ':' name {
              print_debug("component_specification", 1, 1);
              $$ = NULL;
              free($3);
       }

instantiation_list :
       IDENTIFIER opts_colon_identifier {
              print_debug("instantiation_list", 1, 1);
              $$ = NULL;
              multifree(2, $1, $2);
       }
     | RW_OTHERS { print_debug("instantiation_list", 2, 0); }
     | RW_ALL { print_debug("instantiation_list", 3, 1); }

binding_indication :
       opt_use_entity_aspect opt_generic_map_aspect opt_port_map_aspect {
              print_debug("binding_indication", 1, 1);
              $$ = NULL;
       }

entity_aspect :
       RW_ENTITY name opt_parenthesis_identifier {
              print_debug("entity_aspect", 1, 1);
              $$ = NULL;
              free($2);
       }
     | RW_CONFIGURATION name {
              print_debug("entity_aspect", 2, 1);
              $$ = NULL;
              free($2);
       }
     | RW_OPEN {
              print_debug("entity_aspect", 3, 0);
       }

generic_map_aspect :
       RW_GENERIC RW_MAP '(' association_list ')' {
              print_debug("generic_map_aspect", 1, 1);
              $$ = getstr(3, " #(\n", $4, "\n   )");
              free($4);
       }

port_map_aspect :
       RW_PORT RW_MAP '(' association_list ')' {
              print_debug("port_map_aspect", 1, 1);
              $$ = getstr(3, " (\n", $4, "\n   )");
              free($4);
       }

disconnection_specification :
       RW_DISCONNECT guarded_signal_specification RW_AFTER expression ';' {
              print_debug("disconnection_specification", 1, 1);
              $$ = NULL;
              multifree(2, $2, $4);
              print_support("'disconnect' keyword and its signals are ignored.", INFMSG);
       }

guarded_signal_specification :
       signal_list ':' name {
              print_debug("guarded_signal_specification", 1, 1);
              $$ = NULL;
              multifree(2, $1, $3);
       }

signal_list :
       name opts_name {
              print_debug("signal_list", 1, 1);
              $$ = NULL;
              multifree(2, $1, $2);
       }
     | RW_OTHERS {
              print_debug("signal_list", 2, 1);
              $$ = NULL;
       }
     | RW_ALL {
              print_debug("signal_list", 3, 1);
              $$ = NULL;
       }

name :
       IDENTIFIER     { print_debug("name", 1, 1); }
     | STRING_LITERAL { print_debug("name", 2, 1); }
     | selected_name  { print_debug("name", 3, 1); }
     | indexed_name   { print_debug("name", 4, 0); }
     | slice_name     { print_debug("name", 5, 0); }
     | attribute_name { print_debug("name", 6, 0); }

prefix :
       name { print_debug("prefix", 1, 1); }
     | function_call {
              print_debug("prefix", 2, 0);
       }

suffix :
       IDENTIFIER { print_debug("suffix", 1, 1); }
     | CHARACTER_LITERAL {
              print_debug("suffix", 2, 0);
       }
     | STRING_LITERAL {
              print_debug("suffix", 3, 0);
       }
     | RW_ALL { print_debug("suffix", 4, 1); }

indexed_name :
       prefix '(' expression opts_colon_exp ')' {
              print_debug("indexed_name", 1, 0);
       }

slice_name :
       prefix '(' discrete_range ')' {
              print_debug("slice_name", 1, 0);
       }

attribute_name :
       prefix opt_signature '\'' IDENTIFIER opt_parenthesis_expression {
              print_debug("attribute_name", 1, 0);
       }

expression :
       relation { print_debug("expression", 1, 1); }
     | relation opts_and_relation {
              print_debug("expression", 2, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }
     | relation opts_or_relation {
              print_debug("expression", 3, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }
     | relation opts_xor_relation {
              print_debug("expression", 4, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }
     | relation RW_NAND relation {
              print_debug("expression", 5, 1);
              $$ = getstr(3, $1, " ~& ", $3);
              multifree(2, $1, $3);
       }
     | relation RW_NOR relation {
              print_debug("expression", 6, 1);
              $$ = getstr(3, $1, " ~| ", $3);
              multifree(2, $1, $3);
       }
     | relation opts_xnor_relation {
              print_debug("expression", 7, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

relation :
       shift_expression opt_rel_op_shft_exp {
              print_debug("relation", 1, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

opts_and_relation :
       %empty { print_debug("opts_and_relation", 1, 1); $$ = NULL; }
     | opts_and_relation RW_AND relation {
              print_debug("opts_and_relation", 2, 1);
              $$ = getstr(3, $1, " & ", $3);
              multifree(2, $1, $3);
       }

opts_or_relation :
       %empty { print_debug("opts_or_relation", 1, 1); $$ = NULL; }
     | opts_or_relation RW_OR relation {
              print_debug("opts_or_relation", 2, 1);
              $$ = getstr(3, $1, " | ", $3);
              multifree(2, $1, $3);
       }

opts_xor_relation :
       %empty { print_debug("opts_xor_relation", 1, 1); $$ = NULL; }
     | opts_xor_relation RW_XOR relation {
              print_debug("opts_xor_relation", 2, 1);
              $$ = getstr(3, $1, " ^ ", $3);
              multifree(2, $1, $3);
       }

opts_xnor_relation :
       %empty { print_debug("opts_xnor_relation", 1, 1); $$ = NULL; }
     | opts_xnor_relation RW_XNOR relation {
              print_debug("opts_xnor_relation", 2, 1);
              $$ = getstr(3, $1, " ~^ ", $3);
              multifree(2, $1, $3);
       }

shift_expression :
       simple_expression opt_shft_op_simple_exp {
              print_debug("shift_expression", 1, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

simple_expression :
       sign term opts_adding_op_term {
              print_debug("simple_expression", 1, 1);
              $$ = getstr(3, $1, $2, $3);
              multifree(3, $1, $2, $3);
       }
     | term opts_adding_op_term {
              print_debug("simple_expression", 2, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

term :
       factor { print_debug("term", 1, 1); }
     | term multiplying_operator factor {
              print_debug("term", 2, 1);
              $$ = getstr(3,$1, $2, $3);
              multifree(3,$1, $2, $3);
       }

opts_adding_op_term :
       %empty { print_debug("opts_adding_op_term", 1, 1); $$ = NULL; }
     | opts_adding_op_term adding_operator term {
              print_debug("opts_adding_op_term", 2, 1);
              $$ = getstr(3, $1, $2, $3);
              multifree(3, $1, $2, $3);
       }

factor :
       primary { print_debug("factor", 1, 1); }
     | primary "**" primary {
              print_debug("factor", 2, 1);
              $$ = getstr(3, $1, "**", $3);
              multifree(2, $1, $3);
       }
     | RW_ABS primary {
              print_debug("factor", 3, 1);
              $$ = getstr(7, " ( ( ", $2, "< 0 ) ? -", $2, " : ", $2, " )");
              free($2);
       }
     | RW_NOT primary {
              print_debug("factor", 4, 1);
              $$ = getstr(2, "~", $2);
              free($2);
       }

primary :
       name                  { print_debug("primary", 1, 1);  }
     | ABSTRACT_LITERAL      { print_debug("primary", 2, 1);  }
     | ABSTRACT_LITERAL name {
              print_debug("primary", 3, 1);
              $$ = getstr(3, $1, " `", $2);
              multifree(2, $1, $2);
       }
     | enumeration_literal   { print_debug("primary", 4, 1);  }
     | BIT_STRING_LITERAL    { print_debug("primary", 5, 1);  }
     | RW_NULL               { print_debug("primary", 6, 0);  }
     | aggregate             { print_debug("primary", 7, 1);  }
     | function_call         { print_debug("primary", 8, 0);  }
     | qualified_expression  { print_debug("primary", 9, 0);  }
     | type_conversion       { print_debug("primary", 10, 1); }
     | allocator             { print_debug("primary", 11, 0); }
     | '(' expression ')'    {
              print_debug("primary", 12, 1);
              $$ = getstr(3, "(", $2, ")");
              free($2);
       }

type_conversion :
       name '(' expression ')' {
              print_debug("type_conversion", 1, 1);
              $$ = funccall($1, $3);
              multifree(2, $1, $3);
       }

relational_operator :
       '='  { print_debug("relational_operator", 1, 1); $$ = getstr(1, " == "); }
     | "/=" { print_debug("relational_operator", 2, 1); $$ = getstr(1, " != "); }
     | '<'  { print_debug("relational_operator", 3, 1); $$ = getstr(1, " < ");  }
     | "<=" { print_debug("relational_operator", 4, 1); $$ = getstr(1, " <= "); }
     | '>'  { print_debug("relational_operator", 5, 1); $$ = getstr(1, " > ");  }
     | ">=" { print_debug("relational_operator", 6, 1); $$ = getstr(1, " >= "); }

shift_operator :
       RW_SLL { print_debug("shift_operator", 1, 0); }
     | RW_SRL { print_debug("shift_operator", 2, 0); }
     | RW_SLA { print_debug("shift_operator", 3, 0); }
     | RW_SRA { print_debug("shift_operator", 4, 0); }
     | RW_ROL { print_debug("shift_operator", 5, 0); }
     | RW_ROR { print_debug("shift_operator", 6, 0); }

adding_operator :
       '+' { print_debug("adding_operator", 1, 1); $$ = getstr(1, "+"); }
     | '-' { print_debug("adding_operator", 2, 1); $$ = getstr(1, "-"); }
     | '&' { print_debug("adding_operator", 3, 1); $$ = getstr(1, ","); }

sign :
       '+' { print_debug("sign", 1, 1); $$ = getstr(1, "+"); }
     | '-' { print_debug("sign", 2, 1); $$ = getstr(1, "-"); }

multiplying_operator :
       '*'    { print_debug("multiplying_operator", 1, 1); $$ = getstr(1, "*"); }
     | '/'    { print_debug("multiplying_operator", 2, 1); $$ = getstr(1, "/"); }
     | RW_MOD { print_debug("multiplying_operator", 3, 1); $$ = getstr(1, "%"); }
     | RW_REM { print_debug("multiplying_operator", 4, 1); $$ = getstr(1, "%"); }

aggregate :
       '(' element_association opts_colon_element_assoc ')' {
              print_debug("aggregate", 1, 1);
              $$ = getstr(1, $2);
              if ($3) print_support("'aggregates' is not yet supported, aborting.", ERRMSG);
              multifree(2, $2, $3);
       }

element_association :
       expression {
              print_debug("element_association", 1, 0);
       }
     | choices "=>" expression {
              print_debug("element_association", 2, 1);
              $$ = getstr(1, getval($3));
              free($3);
              if ($1 != NULL) {
                 free($1);
                 print_support("only 'others' is supported here yet, aborting.", ERRMSG);
              }
              free($1);
       }

choices :
       choice { print_debug("choices", 1, 1); }
     | choices '|' choice {
              print_debug("choices", 2, 1);
              $$ = getstr(3, $1, ", ", $3);
              multifree(2, $1, $3);
       }

choice :
       simple_expression { print_debug("choice", 1, 1); }
     | discrete_range    { print_debug("choice", 2, 1); }
     | RW_OTHERS         { print_debug("choice", 3, 1); $$ = NULL; }

function_call :
       name opt_actual_parameter_part {
              print_debug("function_call", 1, 0);
       }

qualified_expression :
       name '\'' '(' expression ')' {
              print_debug("qualified_expression", 1, 0);
       }
     | name '\'' aggregate {
              print_debug("qualified_expression", 2, 0);
       }

allocator :
       RW_NEW subtype_indication {
              print_debug("allocator", 1, 0);
       }
     | RW_NEW qualified_expression {
              print_debug("allocator", 2, 0);
       }

sequence_of_statements :
       opts_sequential_statement { print_debug("sequence_of_statements", 1, 1); }

sequential_statement :
       wait_statement                { print_debug("sequential_statement", 1, 0); }
     | assertion_statement           { print_debug("sequential_statement", 2, 0); }
     | report_statement              { print_debug("sequential_statement", 3, 0); }
     | signal_assignment_statement   { print_debug("sequential_statement", 4, 1); }
     | variable_assignment_statement { print_debug("sequential_statement", 5, 0); }
     | procedure_call_statement      { print_debug("sequential_statement", 6, 0); }
     | if_statement                  { print_debug("sequential_statement", 7, 1); }
     | case_statement                { print_debug("sequential_statement", 8, 1); }
     | loop_statement                { print_debug("sequential_statement", 9, 0); }
     | next_statement                { print_debug("sequential_statement", 10, 0); }
     | exit_statement                { print_debug("sequential_statement", 11, 0); }
     | return_statement              { print_debug("sequential_statement", 12, 0); }
     | null_statement                { print_debug("sequential_statement", 13, 1); }

wait_statement :
       opt_identifier_leader RW_WAIT opt_sensitivity_clause opt_condition_clause opt_timeout_clause ';' {
              print_debug("wait_statement", 1, 0);
       }

opt_sensitivity_clause :
       %empty { print_debug("opt_sensitivity_clause", 1, 0); $$ = NULL; }
     | sensitivity_clause {
              print_debug("opt_sensitivity_clause", 2, 0);
       }

opt_condition_clause :
       %empty { print_debug("opt_condition_clause", 1, 0); $$ = NULL; }
     | condition_clause {
              print_debug("opt_condition_clause", 2, 0);
       }

opt_timeout_clause :
       %empty { print_debug("opt_timeout_clause", 1, 0); $$ = NULL; }
     | timeout_clause {
              print_debug("opt_timeout_clause", 2, 0);
       }

sensitivity_clause :
       RW_ON sensitivity_list {
              print_debug("sensitivity_clause", 1, 0);
       }

sensitivity_list :
       name opts_name {
              print_debug("sensitivity_list", 1, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

condition_clause :
       RW_UNTIL expression {
              print_debug("condition_clause", 1, 0);
       }

timeout_clause :
       RW_FOR expression {
              print_debug("timeout_clause", 1, 0);
       }

assertion_statement :
       opt_identifier_leader assertion ';' {
              print_debug("assertion_statement", 1, 0);
       }

assertion :
       RW_ASSERT expression opt_report_expression opt_severity_expression {
              print_debug("assertion", 1, 0);
       }

opt_report_expression :
       %empty { print_debug("opt_report_expression", 1, 0); $$ = NULL; }
     | RW_REPORT expression {
              print_debug("opt_report_expression", 2, 0);
       }

report_statement :
       opt_identifier_leader RW_REPORT expression opt_severity_expression ';' {
              print_debug("report_statement", 1, 0);
       }

opt_severity_expression :
       %empty { print_debug("opt_severity_expression", 1, 0); $$ = NULL; }
     | RW_SEVERITY expression {
              print_debug("opt_severity_expression", 2, 0);
       }

signal_assignment_statement :
       target "<=" opt_delay_mechanism waveform ';' {
              print_debug("signal_assignment_statement", 1, 1);
              $$ = getstr(5, "         ", $1, " <= ", $4, ";\n");
              multifree(3, $1, $3, $4);
       }
     | IDENTIFIER ':' target "<=" opt_delay_mechanism waveform ';' {
              print_debug("signal_assignment_statement", 2, 1);
              $$ = getstr(5, "         ", $3, " <= ", $6, ";\n");
              multifree(4, $1, $3, $5, $6);
       }

delay_mechanism :
       RW_TRANSPORT {
              print_debug("delay_mechanism", 1, 1);
              $$ = NULL;
              print_support("'transport' keyword is ignored.", INFMSG);
       }
     | opt_reject_exp RW_INERTIAL {
              print_debug("delay_mechanism", 2, 1);
              $$ = NULL;
              print_support("'inertial' keyword is ignored.", INFMSG);
       }

target :
       name      { print_debug("target", 1, 1); }
     | aggregate { print_debug("target", 2, 0); }

waveform :
       waveform_element opts_colon_waveform_element {
              print_debug("waveform", 1, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }
     | RW_UNAFFECTED {
              print_debug("waveform", 2, 1);
              $$ = NULL;
              print_support("'unaffected' keyword is ignored.", INFMSG);
       }

waveform_element :
       expression opt_after_expression {
              print_debug("waveform_element", 1, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }
     | RW_NULL opt_after_expression {
              print_debug("waveform_element", 2, 0);
       }

variable_assignment_statement :
       opt_identifier_leader target ":=" expression ';' {
              print_debug("variable_assignment_statement", 1, 0);
       }

procedure_call_statement :
       opt_identifier_leader procedure_call ';' {
              print_debug("procedure_call_statement", 1, 0);
       }

procedure_call :
       name opt_actual_parameter_part {
              print_debug("procedure_call", 1, 0);
       }

if_statement :
       opt_identifier_leader RW_IF expression RW_THEN sequence_of_statements opts_elsif_condition_then_sequence_of_statements opt_else_sequence_of_statements RW_END RW_IF opt_identifier ';' {
              print_debug("if_statement", 1, 1);
              $$ = getstr(7, "      if (", $3, ") begin\n", $5, $6, $7, "      end\n");
              multifree(6, $1, $3, $5, $6, $7, $10);
       }

opts_elsif_condition_then_sequence_of_statements :
       %empty { print_debug("opts_elsif_condition_then_sequence_of_statements", 1, 1); $$ = NULL; }
     | opts_elsif_condition_then_sequence_of_statements RW_ELSIF expression RW_THEN sequence_of_statements {
              print_debug("opts_elsif_condition_then_sequence_of_statements", 2, 1);
              $$ = getstr(5, $1, "      end else if (", $3, ") begin\n", $5);
              multifree(3, $1, $3, $5);
       }

opt_else_sequence_of_statements :
       %empty { print_debug("opt_else_sequence_of_statements", 1, 1); $$ = NULL; }
     | RW_ELSE sequence_of_statements {
              print_debug("opt_else_sequence_of_statements", 2, 1);
              $$ = getstr(2, "      end else begin\n", $2);
              free($2);
       }

case_statement :
       opt_identifier_leader RW_CASE expression RW_IS case_statement_alternative opts_case_statement_alternative RW_END RW_CASE opt_identifier ';' {
              print_debug("case_statement", 1, 1);
              $$ = getstr(6, "      case (", $3, ")\n", $5, $6, "      endcase\n");
              multifree(5, $1, $3, $5, $6, $9);
       }

case_statement_alternative :
       RW_WHEN choices "=>" sequence_of_statements {
              char *auxstr;
              print_debug("case_statement_alternative", 1, 1);
              if (!$2)
                 auxstr = getstr(1, "default");
              else if (strstr($2, ":"))
                 auxstr = getChoiceRange($2);
              else
                 auxstr = getstr(1, $2);
              $$ = getstr(6, "         ", auxstr, " : ", "begin\n", $4, "         end\n");
              multifree(3, $2, $4, auxstr);
       }

opts_case_statement_alternative :
       %empty { print_debug("opts_case_statement_alternative", 1, 1); $$ = NULL; }
     | opts_case_statement_alternative case_statement_alternative {
              print_debug("opts_case_statement_alternative", 2, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

loop_statement :
       opt_identifier_leader opt_iteration_scheme RW_LOOP sequence_of_statements RW_END RW_LOOP opt_identifier ';' {
              print_debug("loop_statement", 1, 0);
       }

iteration_scheme :
       RW_WHILE expression {
              print_debug("iteration_scheme", 1, 0);
       }
     | RW_FOR parameter_specification {
              print_debug("iteration_scheme", 2, 0);
       }

parameter_specification :
       IDENTIFIER RW_IN discrete_range {
              print_debug("parameter_specification", 1, 0);
       }

next_statement :
       opt_identifier_leader RW_NEXT opt_identifier opt_when_condition ';' {
              print_debug("next_statement", 1, 0);
       }

exit_statement :
       opt_identifier_leader RW_EXIT opt_identifier opt_when_condition ';' {
              print_debug("exit_statement", 1, 0);
       }

return_statement :
       opt_identifier_leader RW_RETURN opt_expression ';' {
              print_debug("return_statement", 1, 0);
       }

null_statement :
       opt_identifier_leader RW_NULL ';' {
              print_debug("null_statement", 1, 1);
              $$ = getstr(1, "         /* NULL */\n");
              free($1);
       }

concurrent_statement :
       block_statement                        { print_debug("concurrent_statement", 1, 1); }
     | process_statement                      { print_debug("concurrent_statement", 2, 1); }
     | concurrent_procedure_call_statement    { print_debug("concurrent_statement", 3, 0); }
     | concurrent_assertion_statement         { print_debug("concurrent_statement", 4, 0); }
     | concurrent_signal_assignment_statement { print_debug("concurrent_statement", 5, 1); }
     | component_instantiation_statement      { print_debug("concurrent_statement", 6, 1); }
     | generate_statement                     { print_debug("concurrent_statement", 7, 0); }

block_statement :
       IDENTIFIER ':' RW_BLOCK opt_parenthesis_expression opt_is block_header block_declarative_part RW_BEGIN block_statement_part RW_END RW_BLOCK opt_identifier ';' {
              print_debug("block_statement", 1, 1);
              $$ = getstr(2, $7, $9);
              multifree(7, $1, $4, $5, $6, $7, $9, $12);
       }

block_header :
       opt_generic_things opt_port_things {
              print_debug("block_header", 1, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

opt_generic_things :
       %empty { print_debug("opt_generic_things", 1, 1); $$ = NULL; }
     | generic_clause opt_generic_map_aspect_colon {
              print_debug("opt_generic_things", 2, 0);
       }

opt_generic_map_aspect_colon :
       %empty { print_debug("opt_generic_map_aspect_colon", 1, 0); $$ = NULL; }
     | generic_map_aspect ';' {
              print_debug("opt_generic_map_aspect_colon", 2, 0);
       }

opt_port_things :
       %empty { print_debug("opt_port_things", 1, 1); $$ = NULL; }
     | port_clause opt_port_map_aspect_colon {
              print_debug("opt_port_things", 2, 0);
       }

opt_port_map_aspect_colon :
       %empty { print_debug("opt_port_map_aspect_colon", 1, 0); $$ = NULL; }
     | port_map_aspect ';' {
              print_debug("opt_port_map_aspect_colon", 2, 0);
       }

block_declarative_part :
       opts_block_declarative_item { print_debug("block_declarative_part", 1, 1); }

block_statement_part :
       opts_concurrent_statement { print_debug("block_statement_part", 1, 1); }

process_statement :
       opt_identifier_leader opt_postponed RW_PROCESS opt_parenthesis_sensitivity_list opt_is process_declarative_part RW_BEGIN process_statement_part RW_END opt_postponed RW_PROCESS opt_identifier ';' {
              print_debug("process_statement", 1, 1);
              $$ = getstr(8, "   always ", $4, " begin\n", "      /*TODO:", $6, "*/\n", $8, "   end\n\n");
              multifree(3, $4, $6, $8);
       }

opt_parenthesis_sensitivity_list :
       %empty { print_debug("opt_parenthesis_sensitivity_list", 1, 0); $$ = NULL; }
     | '(' sensitivity_list ')' {
              print_debug("opt_parenthesis_sensitivity_list", 2, 1);
              $$ = getstr(3, "@ (", $2, ")");
              free($2);
       }

process_declarative_part :
       %empty { print_debug("process_declarative_part", 1, 1); $$ = NULL; }
     | process_declarative_part process_declarative_item {
              print_debug("process_declarative_part", 2, 0);
       }

process_declarative_item :
       subprogram_declaration     { print_debug("process_declarative_item", 1, 0); }
     | subprogram_body            { print_debug("process_declarative_item", 2, 0); }
     | type_declaration           { print_debug("process_declarative_item", 3, 0); }
     | subtype_declaration        { print_debug("process_declarative_item", 4, 0); }
     | constant_declaration       { print_debug("process_declarative_item", 5, 0); }
     | variable_declaration       { print_debug("process_declarative_item", 6, 0); }
     | file_declaration           { print_debug("process_declarative_item", 7, 0); }
     | alias_declaration          { print_debug("process_declarative_item", 8, 0); }
     | attribute_declaration      { print_debug("process_declarative_item", 9, 0); }
     | attribute_specification    { print_debug("process_declarative_item", 10, 0); }
     | use_clause                 { print_debug("process_declarative_item", 11, 0); }
     | group_template_declaration { print_debug("process_declarative_item", 12, 0); }
     | group_declaration          { print_debug("process_declarative_item", 13, 0); }

process_statement_part :
       opts_sequential_statement { print_debug("process_statement_part", 1, 1); }

concurrent_procedure_call_statement :
       opt_identifier_leader opt_postponed procedure_call ';' {
              print_debug("concurrent_procedure_call_statement", 1, 0);
       }

concurrent_assertion_statement :
       opt_identifier_leader opt_postponed assertion ';' {
              print_debug("concurrent_assertion_statement", 1, 0);
       }

concurrent_signal_assignment_statement :
       conditional_signal_assignment {
              print_debug("concurrent_signal_assignment_statement", 1, 1);
       }
     | RW_POSTPONED conditional_signal_assignment {
              print_debug("concurrent_signal_assignment_statement", 2, 1);
              $$ = $2;
              print_support("'postponed' keyword is ignored.", INFMSG);
       }
     | IDENTIFIER ':' conditional_signal_assignment {
              print_debug("concurrent_signal_assignment_statement", 3, 1);
              $$ = $3;
       }
     | IDENTIFIER ':' RW_POSTPONED conditional_signal_assignment {
              print_debug("concurrent_signal_assignment_statement", 4, 1);
              $$ = $4;
              print_support("'postponed' keyword is ignored.", INFMSG);
       }
     | opt_identifier_leader opt_postponed selected_signal_assignment {
              print_debug("concurrent_signal_assignment_statement", 5, 1);
              $$ = $3;
       }

options :
       opt_guarded opt_delay_mechanism {
              print_debug("options", 1, 1);
              $$ = NULL;
              multifree(2, $1, $2);
       }

conditional_signal_assignment :
       target "<=" options conditional_waveforms ';' {
              print_debug("conditional_signal_assignment", 1, 1);
              if ($4)
                 $$ = getstr(5, "   assign ", $1, " = ", $4, ";\n\n");
              else
                 $$ = NULL;
              multifree(3, $1, $3, $4);
       }

conditional_waveforms :
       opts_waveform_when_condition_else waveform opt_when_condition {
              print_debug("conditional_waveforms", 1, 1);
              $$ = getstr(3, $1, $2, $3);
              multifree(3, $1, $2, $3);
       }

opts_waveform_when_condition_else :
       %empty { print_debug("opts_waveform_when_condition_else", 1, 1); $$ = NULL; }
     | opts_waveform_when_condition_else waveform RW_WHEN expression RW_ELSE {
              print_debug("opts_waveform_when_condition_else", 2, 1);
              $$ = getstr(6, $1, "( ", $4, " ) ? ", $2 , " : ");
              multifree(3, $1, $2, $4);
       }

selected_signal_assignment :
       RW_WITH expression RW_SELECT target "<=" options selected_waveforms ';' {
              char *auxstr;
              print_debug("selected_signal_assignment", 1, 1);
              auxstr = replace($7,"<SIGNAL>", $4);
              $$ = getstr(6, "   always @ (*)\n", "      case (", $2, ")\n", auxstr, "      endcase\n\n");
              multifree(5, $2, $4, $6, $7, auxstr);
       }

selected_waveforms :
       opts_waveform_when_choices_colon waveform RW_WHEN choices {
              print_debug("selected_waveforms", 1, 1);
              $$ = getstr(4, $1, "         default : <SIGNAL> = ", $2, ";\n");
              multifree(3, $1, $2, $4);
       }

component_instantiation_statement :
       IDENTIFIER ':' instantiated_unit opt_generic_map_aspect opt_port_map_aspect ';' {
              print_debug("component_instantiation_statement", 1, 1);
              $$ = getstr(7, "   ", $3, " ", $1, $4, $5, ";\n\n");
              multifree(4, $1, $3, $4, $5);
       }

instantiated_unit :
       name { print_debug("instantiated_unit", 1, 1); }
     | RW_COMPONENT name {
              print_debug("instantiated_unit", 2, 1);
              $$ = getstr(1, $2);
              free($2);
       }
     | RW_ENTITY name opt_parenthesis_identifier {
              print_debug("instantiated_unit", 3, 1);
              $$ = getstr(1, $2);
              multifree(2, $2, $3);
       }
     | RW_CONFIGURATION name { print_debug("instantiated_unit", 4, 0); }

generate_statement :
       IDENTIFIER ':' generation_scheme RW_GENERATE opt_block_declarative_item_begin opts_concurrent_statement RW_END RW_GENERATE opt_identifier ';' {
              print_debug("generate_statement", 1, 0);
       }

opt_block_declarative_item_begin :
       %empty { print_debug("opt_block_declarative_item_begin", 1, 0); }
     | opts_block_declarative_item RW_BEGIN {
              print_debug("opt_block_declarative_item_begin", 2, 0);
       }

generation_scheme :
       RW_FOR parameter_specification {
              print_debug("generation_scheme", 1, 0);
       }
     | RW_IF expression {
              print_debug("generation_scheme", 2, 0);
       }

opts_name :
       %empty { print_debug("opts_name", 1, 1); $$ = NULL; }
     | opts_name ',' name {
              print_debug("opts_name", 2, 1);
              $$ = getstr(3, $1, ", ", $3);
              multifree(2, $1, $3);;
       }

opts_sequential_statement :
       %empty { print_debug("opts_sequential_statement", 1, 1); $$ = NULL; }
     | opts_sequential_statement sequential_statement {
              print_debug("opts_sequential_statement", 2, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

opts_use_clause :
       %empty { print_debug("opts_use_clause", 1, 1); $$ = NULL; }
     | opts_use_clause use_clause {
              print_debug("opts_use_clause", 2, 0);
       }

opts_configuration_item :
       %empty                                     { print_debug("opts_configuration_item", 1, 1); $$ = NULL; }
     | opts_configuration_item configuration_item { print_debug("opts_configuration_item", 2, 1); }

opts_colon_name :
       %empty { print_debug("opts_colon_name", 1, 0); $$ = NULL; }
     | opts_colon_name ',' name {
              print_debug("opts_colon_name", 2, 0);
       }

opts_colon_index_subtype_def :
       %empty { print_debug("opts_colon_index_subtype_def", 1, 0); $$ = NULL; }
     | opts_colon_index_subtype_def ',' index_subtype_definition {
              print_debug("opts_colon_index_subtype_def", 2, 0);
       }

opts_colon_identifier :
       %empty { print_debug("opts_colon_identifier", 1, 1); $$ = NULL; }
     | opts_colon_identifier ',' IDENTIFIER { print_debug("opts_colon_identifier", 2, 1); }

opts_colon_exp :
       %empty { print_debug("opts_colon_exp", 1, 0); $$ = NULL; }
     | opts_colon_exp ',' expression {
              print_debug("opts_colon_exp", 2, 0);
       }

opts_colon_element_assoc :
       %empty { print_debug("opts_colon_element_assoc", 1, 1); $$ = NULL; }
     | opts_colon_element_assoc ',' element_association {
              print_debug("opts_colon_element_assoc", 2, 0);
       }

opts_colon_waveform_element :
       %empty { print_debug("opts_colon_waveform_element", 1, 1); $$ = NULL; }
     | opts_colon_waveform_element ',' waveform_element {
              print_debug("opts_colon_waveform_element", 2, 0);
       }

opts_waveform_when_choices_colon :
       %empty { print_debug("opts_waveform_when_choices_colon", 1, 1); $$ = NULL; }
     | opts_waveform_when_choices_colon waveform RW_WHEN choices ',' {
              print_debug("opts_waveform_when_choices_colon", 2, 1);
              $$ = getstr(6, $1, "         ", $4, " : <SIGNAL> = ", $2, ";\n");
              multifree(3, $1, $2, $4);
       }

opt_assign_exp :
       %empty { print_debug("opt_assign_exp", 1, 1); $$ = NULL; }
     | ":=" expression {
              print_debug("opt_assign_exp", 2, 1);
              $$ = getstr(2, " = ", getval($2));
              free($2);
       }

opt_actual_parameter_part :
       %empty { print_debug("opt_actual_parameter_part", 1, 0); $$ = NULL; }
     | '(' association_list ')' {
              print_debug("opt_actual_parameter_part", 2, 0);
       }

opt_parenthesis_expression :
       %empty             { print_debug("opt_parenthesis_expression", 1, 0); $$ = NULL; }
     | '(' expression ')' { print_debug("opt_parenthesis_expression", 2, 1); $$ = NULL; }

opt_parenthesis_identifier :
       %empty { print_debug("opt_parenthesis_identifier", 1, 1); $$ = NULL; }
     | '(' IDENTIFIER ')' {
              print_debug("opt_parenthesis_identifier", 2, 1);
              $$ = $2;
       }

opt_after_expression :
       %empty { print_debug("opt_after_expression", 1, 1); $$ = NULL; }
     | RW_AFTER expression {
              print_debug("opt_after_expression", 2, 1);
              $$ = NULL;
              free($2);
              print_support("'after' keyword and its value are ignored.", INFMSG);
       }

opt_when_condition :
       %empty { print_debug("opt_when_condition", 1, 1); $$ = NULL; }
     | RW_WHEN expression {
              print_debug("opt_when_condition", 2, 0);
       }

opt_delay_mechanism :
       %empty          { print_debug("opt_delay_mechanism", 1, 1); $$ = NULL; }
     | delay_mechanism { print_debug("opt_delay_mechanism", 2, 1); }

opt_identifier :
       %empty     { print_debug("opt_identifier", 1, 1); $$ = NULL; }
     | IDENTIFIER { print_debug("opt_identifier", 2, 1); }

opt_identifier_leader :
       %empty { print_debug("opt_identifier_leader", 1, 1); $$ = NULL; }
     | IDENTIFIER ':' {
              print_debug("opt_identifier_leader", 2, 1);
       }

opt_signature :
       %empty { print_debug("opt_signature", 1, 0); $$ = NULL; }
     | '[' opt_name_colon_name opt_return_name ']' {
              print_debug("opt_signature", 2, 0);
       }

opt_begin_entity_statement_part :
       %empty { print_debug("opt_begin_entity_statement_part", 1, 1); $$ = NULL; }
     | RW_BEGIN entity_statement_part {
              print_debug("opt_begin_entity_statement_part", 2, 0);
       }

opt_binding_indication :
       %empty { print_debug("opt_binding_indication", 1, 1); $$ = NULL; }
     | binding_indication ';' { print_debug("opt_binding_indication", 2, 1); }

opt_block_config :
       %empty { print_debug("opt_block_config", 1, 1); $$ = NULL; }
     | block_configuration {
              print_debug("opt_block_config", 2, 0);
       }

opt_subprogram_kind :
       %empty          { print_debug("opt_subprogram_kind", 1, 1); $$ = NULL; }
     | subprogram_kind { print_debug("opt_subprogram_kind", 2, 0); }

opt_name_colon_name :
       %empty { print_debug("opt_name_colon_name", 1, 0); $$ = NULL; }
     | name opts_colon_name {
              print_debug("opt_name_colon_name", 2, 0);
       }

opt_return_name :
       %empty { print_debug("opt_return_name", 1, 0); $$ = NULL; }
     | RW_RETURN name {
              print_debug("opt_return_name", 2, 0);
       }

opt_file_open_information :
       %empty { print_debug("opt_file_open_information", 1, 0); $$ = NULL; }
     | file_open_information {
              print_debug("opt_file_open_information", 2, 0);
       }

opt_open_expression :
       %empty { print_debug("opt_open_expression", 1, 0); $$ = NULL; }
     | RW_OPEN expression {
              print_debug("opt_open_expression", 2, 0);
       }

opt_formal_part_arrow :
       %empty { print_debug("opt_formal_part_arrow", 1, 1); $$ = NULL; }
     | formal_part "=>" { print_debug("opt_formal_part_arrow", 2, 1); }

opt_subtype_indication :
       %empty { print_debug("opt_subtype_indication", 1, 0); $$ = NULL; }
     | ':' subtype_indication {
              print_debug("opt_subtype_indication", 2, 0);
       }

opt_use_entity_aspect :
       %empty { print_debug("opt_use_entity_aspect", 1, 0); $$ = NULL; }
     | RW_USE entity_aspect { print_debug("opt_use_entity_aspect", 2, 1); }

opt_rel_op_shft_exp :
       %empty { print_debug("opt_rel_op_shft_exp", 1, 1); $$ = NULL; }
     | relational_operator shift_expression {
              print_debug("opt_rel_op_shft_exp", 2, 1);
              $$ = getstr(2, $1, $2);
              multifree(2, $1, $2);
       }

opt_shft_op_simple_exp :
       %empty { print_debug("opt_shft_op_simple_exp", 1, 1); $$ = NULL; }
     | shift_operator simple_expression {
              print_debug("opt_shft_op_simple_exp", 2, 0);
       }

opt_reject_exp :
       %empty { print_debug("opt_reject_exp", 1, 0); $$ = NULL; }
     | RW_REJECT expression {
              print_debug("opt_reject_exp", 2, 1);
              $$ = NULL;
              free($2);
              print_support("'reject' keyword and its value are ignored.", INFMSG);
       }

opt_iteration_scheme :
       %empty { print_debug("opt_iteration_scheme", 1, 0); $$ = NULL; }
     | iteration_scheme {
              print_debug("opt_iteration_scheme", 2, 0);
       }

opt_expression :
       %empty { print_debug("opt_expression", 1, 0); $$ = NULL; }
     | expression {
              print_debug("opt_expression", 2, 0);
       }

%%

int main(int argc, char **argv) {
   int option;

   static struct option long_options[] = {
      {"verbose", no_argument, NULL, 'v'},
      {"quiet",   no_argument, NULL, 'q'},
      {NULL,      0,           NULL,  0 }
   };

   while ((option = getopt_long(argc, argv, "qv", long_options, NULL)) != -1) {
      switch (option) {
         case 'q': debug--; break;
         case 'v': debug++; break;
      }
   }

   yyparse();
   return 0;
}
