/*
  vhdl2verilog, a compiler from synthesizable VHDL to Verilog
  Copyright (C) 2017-2019, Rodrigo A. Melo

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "support.h"

#define ALLOC 1000

extern int yylineno;
extern char *yytext;

extern int count;
extern int debug;

void yyerror(const char *s) {
   fprintf(stderr, "ERROR:%d: %s at '%s'\n", yylineno, s, yytext); exit(1);
}

/******************************************************************************
String (memory allocation) related functions
******************************************************************************/

char *convtype(const char *type, const char *range) {
   char *auxstr;
   if (!strcmp(type, "integer") || !strcmp(type, "natural") || !strcmp(type, "positive"))
      auxstr = getstr(2, "integer ", range);
   else if (!strcmp(type, "signed") || !strcmp(type, "real") || !strcmp(type, "time"))
      auxstr = getstr(3, type, " ", range);
   else if (
      !strcmp(type, "bit") || !strcmp(type, "bit_vector") ||
      !strcmp(type, "std_logic") || !strcmp(type, "std_logic_vector") ||
      !strcmp(type, "unsigned") || !strcmp(type, "boolean") ||
      !strcmp(type, "std_ulogic") || !strcmp(type, "std_ulogic_vector") ||
      !strcmp(type, "string") || !strcmp(type, "character")
   )
      auxstr = getstr(1, range);
   else
      auxstr = getstr(3, "`", type, " ");
   return auxstr;
}

int elements(const char *s, const char sep) {
   int i, cnt = 0;
   for (i = 0; i < strlen(s); i++)
       if (s[i] == sep) cnt++;
   return cnt + 1;
}

char *funccall(const char *name, const char *exp) {
   char *auxstr;
   if (!strcmp(name, "rising_edge")) {
      auxstr = getstr(2, exp, " == '1'");
   } else if (!strcmp(name, "falling_edge")) {
      auxstr = getstr(2, exp, " == '0'");
   } else {
      auxstr = getstr(4, name, "(", exp, ")");
   }
   return auxstr;
}

/*TODO: improve memory ALLOC*/
char *getChoiceRange(char *s) {
   char *token;
   char *newstr;
   char auxstr[6];
   int i, from, to, aux;
   //
   token = strtok(s, ":");
   from = atoi(token);
   token = strtok(NULL, ":");
   to = atoi(token);
   if (from > to) { aux = from; from = to; to = aux; }
   //
   newstr = (char *) malloc(ALLOC);
   if (!newstr) { perror("malloc"); exit(2); }
   //
   strcpy(newstr, "");
   sprintf(auxstr,"%d", from);
   strcat(newstr, auxstr);
   for (i = from+1; i <= to; i++) {
       sprintf(auxstr,", %d", i);
       strcat(newstr, auxstr);
   }
   return newstr;
}

char *getval(char *val) {
   char *newstr;
   int i;
   newstr = (char *) malloc(strlen(val) + 1);
   if (!newstr) { perror("malloc"); exit(2); }
   if (!strcmp(val, "'0'"))
      strcpy(newstr,"0");
   else if (!strcmp(val, "'1'"))
      strcpy(newstr,"~0");
   else if (val[0] == '"') {
      strcpy(newstr, "'b");
      for (i = 1; i < strlen(val)-1; i++) newstr[i+1] = val[i];
      newstr[i+1] = '\0';
   } else if (val[0] == 'b') {
      strcpy(newstr, "'b");
      for (i = 2; i < strlen(val)-1; i++) newstr[i] = val[i];
      newstr[i] = '\0';
   } else if (val[0] == 'o') {
      strcpy(newstr, "'o");
      for (i = 2; i < strlen(val)-1; i++) newstr[i] = val[i];
      newstr[i] = '\0';
   } else if (val[0] == 'x') {
      strcpy(newstr, "'h");
      for (i = 2; i < strlen(val)-1; i++) newstr[i] = val[i];
      newstr[i] = '\0';
   } else if (!strcmp(val,"FALSE")) {
      strcpy(newstr,"1'b0");
   } else if (!strcmp(val,"TRUE")) {
      strcpy(newstr,"1'b1");
   } else 
      strcpy(newstr, val);
   return newstr;
}

char *getstr(int num, ...) {
   va_list ap;
   char *newstr, *auxstr;
   int i, len;
   //
   va_start(ap, num);
   len = 1;
   for (i = 0; i < num; i++)
       if (auxstr = va_arg(ap, char *))
          len += strlen(auxstr);
   va_end(ap);
   //
   newstr = (char *) malloc(len);
   if (!newstr) { perror("malloc"); exit(2); }
   //
   va_start(ap, num);
   strcpy(newstr, "");
   for (i = 0; i < num; i++)
       if (auxstr = va_arg(ap, char *))
          strcat(newstr, auxstr);
   va_end(ap);
   //
   if (!strcmp(newstr, "")) {
      free(newstr);
      newstr = NULL;
   }
   //
   return newstr;
}

char *itoa(int integer) {
   char *newstr;
   newstr = (char *) malloc(5);
   if (!newstr) { perror("malloc"); exit(2); }
   sprintf(newstr,"%d", integer);
   return newstr;
}

void multifree(int num, ...) {
   va_list ap;
   char *auxstr;
   int i;
   //
   va_start(ap, num);
   for (i = 0; i < num; i++)
       if (auxstr = va_arg(ap, char *))
          free(auxstr);
   va_end(ap);
}

// Based on replaceWord from
// https://www.geeksforgeeks.org/c-program-replace-word-text-another-given-word/
char *replace(const char *s, const char *old, const char *new) { 
    char *result; 
    int i, cnt = 0; 
    int newlen = strlen(new);
    int oldlen = strlen(old);
    //
    for (i = 0; s[i] != '\0'; i++) {
        if (strstr(&s[i], old) == &s[i]) {
            cnt++; 
            i += oldlen - 1;
        }
    }
    //
    result = (char *)malloc(i + cnt * (newlen - oldlen) + 1);
    if (!result) { perror("malloc"); exit(2); }
    //
    i = 0; 
    while (*s) {
       if (strstr(s, old) == s) {
            strcpy(&result[i], new);
            i += newlen;
            s += oldlen;
       } else
            result[i++] = *s++;
    }
    //
    result[i] = '\0';
    return result;
}

/******************************************************************************
General purpose functions
******************************************************************************/

int clog2(int arg) {
   int i;
   for (i = 0; i < 32; i++)
       if (arg <= power(2, i))
          return i;
   return 32;
}

int power(int base, int exp) {
   int res = 1;
   while(exp) {
      res *= base;
      exp--;
   }
   return res;
}

/******************************************************************************
Print functions
******************************************************************************/

void print_debug(const char *msg, int opt, int severity) {
   if (debug >= 0 & severity == 0)
      fprintf(stderr, "WARNING:%d: feature not yet implemented (%s[%d])\n", yylineno, msg, opt);
   else if (debug > 0 & severity == 1)
      fprintf(stderr, "* %s[%d]\n", msg, opt);
}

void print_support(const char *msg, int severity) {
   if (debug >= 0) {
      if (severity == ERRMSG) {
         fprintf(stderr, "ERROR:%d: %s\n", yylineno, msg);
         exit(2);
      }
      else if (severity == WARMSG)
         fprintf(stderr, "WARNING:%d: %s\n", yylineno, msg);
      else if (severity == INFMSG)
         fprintf(stderr, "INFO:%d: %s\n", yylineno, msg);
      else
         fprintf(stderr, "NOTE:%d: %s\n", yylineno, msg);
   }
}
