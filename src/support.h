/*
  vhdl2verilog, a compiler from synthesizable VHDL to Verilog
  Copyright (C) 2017-2019, Rodrigo A. Melo

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SUPPORT
#define SUPPORT

#define ERRMSG 0
#define WARMSG 1
#define INFMSG 2
#define NOTMSG 3

int yylex();
void yyerror(const char *);

char *convtype(const char *, const char *);
int elements(const char *, const char);
char *funccall(const char *, const char *);
char *getChoiceRange(char *);
char *getval(char *);
char *getstr(int, ...);
char *itoa(int);
void multifree(int, ...);
char *replace(const char *, const char *, const char *);

int clog2(int);
int power(int, int);

void print_debug(const char *, int, int);
void print_support(const char *, int);

#endif
