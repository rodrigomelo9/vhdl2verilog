# Features

## Design Units

* Entity : only the generic and port clauses.
* Architecture : only one (the first) per entity.
* Package : not yet supported.
* Package Body : not yet supported.
* Configuration : not supported.

## Modes

VHDL    | Verilog | Comments
---     | ---     | ---
*empty* | input   | If no mode is explicitly given, mode IN is assumed.
in      | input   |
out     | output  |
inout   | inout   |
buffer  | inout   | Is an output that can be read.
linkage | inout   | Can be read or updated.

## Predefined Types/subtypes

VHDL     | Verilog
---      | ---
signed   | signed
integer  | integer
natural  | integer
positive | integer
real     | real
time     | time
OTHERS   | *nothing*

* OTHERS : bit, bit_vector, std_logic, std_logic_vector, unsigned, std_ulogic, std_ulogic_vector, boolean, string, character.

## Ranges

VHDL              | Verilog
---               | ---
(0 to 7)          | [0:7]
(7 downto 0)      | [7:0]
range 0 to 10     | [$clog2(10)-1:0]
range 5 to 10     | [$clog2(10)-1:0]
range -1 to 10    | [$clog2(10):0]
range -20 to 5    | [$clog2(20):0]
*string*(1 to 10) | [10*8:1]
*character*       | [8:1]
OTHERS            | *nothing*

* OTHERS: bit, std_logic, std_ulogic, boolean, integer, natural, positive.

* Note: $clog2 is not part of Verilog 2001 (was used here to specify the math op).

## Constants and Generics

VHDL     | Verilog
---      | ---
constant | localparam
generic  | parameter

* Note: unconstrained strings seems not allowed in both standards, however, some tools support them as generic/parameter and constant/localparam.

# Type/subtypes declarations

### Enumerations

VHDL:
```
type type1 is (STATE1, STATE2, STATE3);
signal states : type1;
```

Verilog:
```
localparam STATE1 = 0, STATE2 = 1, STATE3 = 2;
`define type1
wire `type1 states;
```

### Arrays

VHDL:
```
type array1 is array (0 to 3)           of std_logic;
type array2 is array (3 downto 0)       of std_logic_vector(7 downto 0);
type array3 is array (natural range <>) of std_logic_vector(7 downto 0);
type array4 is array (0 to 9, 0 to 7)   of std_logic_vector(7 downto 0);
signal signal1 : array1;
signal signal2 : array2;
signal signal3 : array3(0 to9);
signal signal4 : array4;
```

Verilog:
```
`define array1(name) name[0:3]
`define array2(name) [7:0]name[3:0]
`define array3(name) [7:0]name
`define array4(name) [7:0]name[0:9][0:7]
wire `array1(signal1);
wire `array2(signal2);
wire `array3(signal3)[0:9];
wire `array4(signal4);
```

## Operators

VHDL     | Verilog
---      | ---
NOT A    | ~A *(bit-wise)*, !A *(logical)*
A AND B  | A & B *(bit-wise)*, A && B *(logical)*
A OR B   | A \| B *(bit-wise)*, A \|\| B *(logical)*
A XOR B  | A ^ B *(bit-wise)*
A NAND B | A ~& B *(bit-wise)*, !(A && B) *(logical)*
A NOR B  | A ~\| B *(bit-wise)*, !(A \|\| B) *(logical)*
A XNOR B | A ~^ B *(bit-wise)*
ABS A    | ( ( A < 0 ) ? -A : A )
A REM B  | A % B
A MOD B  | A % B *(?)*
A SLL B  | A << B
A SRL B  | A >> B
A SLA B  | A <<< B
A SRA B  | A >>> B
A ROR B  | *???*
A ROL B  | *???*
A & B    | {A, B}

## Numbers

VHDL            | Verilog
---             | ---
'0'             | 1'b0
'1'             | 1'b1
"00000000"      | 8'b00000000
"01101001"      | 8,b01101001
b"11000101"     | 8'b11000101
o"151"          | 8,o151
x"A2"           | 8'hA2
(others => '0') | { n {1'b0} }
(others => '1') | { n {1'b1} }
NUMBER          | NUMBER

## Statements

### When/else (concurrent)

VHDL:
```
signal1 <= expression1 when condition1 else
           expression2 when condition2 else
           expression3 ;
```

Verilog:
```
assign signal1 = ( conditional1 ) ? expression1 :
                 ( conditional2 ) ? expression2 :
                 expression3;
```

### With/select (concurrent)

VHDL:
```
with expression select signal1 <=
   expression1 when value1,
   expression2 when value2,
   expression3 when others;
```

Verilog:
```
case (expression)
   value1  : signal1 <= expression1;
   value2  : signal1 <= expression2;
   default : signal1 <= expression3;
endcase
```

### If / elsif / else (sequential)

VHDL:
```
if condition1 then
   statements1;
elsif condition2 then
   statements2;
else
   statements3;
end if;
```

Verilog:
```
if (condition1) begin
   statements1;
end else if (condition2) begin
   statements2;
end else begin
   statements3;
end
```

### Case (sequential)

VHDL:
```
case op is
   when value1           => statements1;
   when value2 | value3  => statements2;
   when value4 to value6 => statements3;
   when others           => statements4;
end case;
```

Verilog:
```
case (expression)
   value1                 : begin statements1; end
   value2, value3         : begin statements2; end
   value4, value5, value6 : begin statements4; end
   default                : begin statements4; end
endcase
```

## Ignored

* after, bus, disconnect, guarded, inertial, register, reject, postponed, transport, unaffected.
