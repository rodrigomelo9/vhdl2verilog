# Example

A small example which I use to test new features about the use of Flex and Bison.

Execute `make run` and try things like:
```
4<9
4>9
44<=99
44=>99
```
Other things (not number, other symbols) will abort with `error: syntax error`.
