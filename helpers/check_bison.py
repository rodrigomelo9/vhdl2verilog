#!/usr/bin/python
#
# Copyright (C) 2019, Rodrigo A. Melo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys, re

text = sys.stdin.read()

def check_variadic_function(text):
    file = text.split('\n')
    lineno = 0
    for line in file:
        lineno += 1
        line = re.sub(r"(\"[^\"]*\")", "", line)
        matches = re.findall(r"getstr\(([^\)]*)\)", line)
        if not matches:
           matches = re.findall(r"multifree\(([^\)]*)\)", line)
        if matches:
           match = matches[0]
           args = re.findall(r"(\d*)", match)
           args = 0 if args[0] == '' else int(args[0])
           colons = match.count(',')
           if args and args != colons:
              print("\nMISSMATCH:%d: NUM_OF_ARGS (%d) != NUM_OF_COLONS (%d)\n" % (lineno, args, colons))

check_variadic_function(text)
