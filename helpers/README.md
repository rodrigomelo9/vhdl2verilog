# Helpers and how to use it

## e2bnf.py

Converts from the Extended Backus-Naur Form (EBNF) used in VHDL93 to the BNF used in Bison.

Transformations:
* Adds Single Quote to literal character tokens.
* Adds Double Quotes to literal string tokens.
* Optional rules are made explicit:
  * [] means 0 or 1 time. Convert every option [ E ] to a fresh non-terminal X and add `X = empty | E`
  * {} means 0 or more times. Convert every repetition { E } to a fresh non-terminal X and add `X = empty | X E`

Examples:
* `aaa ::= [ bbb ] ccc` is replaced by
```
aaa ::= aaa_opt1 ccc
aaa_opt1 ::= | bbb
```
* `aaa ::= { bbb } ccc` is replace by
```
aaa ::= aaa_opt1 ccc
aaa_opt1 ::= | aaa_opt1 bbb
```
* The special case `aaa ::= { bbb }` is replace by `aaa ::= | aaa bbb`

Usage example: `cat ../doc/vhdl93.bnf | python e2bnf.py`

## bnf2code.py

Gets code to be used in Flex and Bison, from the output of e2bnf.py.

Text transformations:
* Adds RW_ prefix to Reserved Words to avoid name collision (as example, BEGIN is a Flex action).
* Adds explicit empty indications.
* Makes little transformations and indentation.
* Adds small C code to print debug info.

It also gives some statics to know if there are unused rules.

Usage example: `cat ../doc/vhdl93.bnf | python e2bnf.py | python bnf2code.py`
