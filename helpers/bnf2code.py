#!/usr/bin/python
#
# Copyright (C) 2017-2019, Rodrigo A. Melo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys, re

text = sys.stdin.read()

# Functions ###################################################################

def printFlexCode(text):
    reserved = re.findall("[A-Z]{2,}", text)        # Catching reserved words
    reserved = list(set( reserved ))                # Deleting duplicates
    reserved.sort()
    print ("Reserved words: %d\n" % len(reserved))
    for word in reserved:
        print ("%-30s{ return RW_%-14s }" % ("\"" + word.lower() + "\"", word + ";") )

def removeRules(text):
    rules = [
        "abstract_literal",
        "base",
        "base_specifier",
        "based_integer",
        "based_literal",
        "basic_character",
        "basic_graphic_character",
        "basic_identifier",
        "bit_string_literal",
        "bit_value",
        "character_literal",
        "decimal_literal",
        "declaration",
        "digit",
        "exponent",
        "extended_digit",
        "extended_identifier",
        "full_instance_based_path",
        "full_path_instance_element",
        "full_path_to_instance",
        "generate_label",
        "graphic_character",
        "identifier",
        "instance_based_path",
        "instance_name",
        "integer",
        "leader",
        "letter",
        "letter_or_digit",
        "local_item_name",
        "logical_operator",
        "miscellaneous_operator",
        "object_declaration",
        "package_based_path",
        "path_instance_element",
        "path_name",
        "path_to_instance",
        "process_label",
        "shared_curly_braces_02",
        "shared_curly_braces_05",
        "shared_square_brackets_12",
        "shared_square_brackets_17",
        "shared_square_brackets_23",
        "square_brackets_52",
        "square_brackets_53",
        "square_brackets_54",
        "square_brackets_55",
        "square_brackets_56",
        "square_brackets_57",
        "string_literal"
    ]
    for element in sorted(rules):
        text = re.sub(r"\n%s .*::=.*\n" % (element), "" , text)
    return text

def printBisonCode(text):
    text = re.sub(r"([A-Z]{2,})" , r"RW_\1"        , text) # Add RW_ prefix to Reserved Words
    text = re.sub(r"::= \|"      , r"::= %empty |" , text)
    text = re.sub(r" \| "        , r"\n     | "    , text)
    text = re.sub(r"::= "        , r":\n       "   , text)
    # replaces
    lexical_elements = [
        "identifier", "abstract_literal", "character_literal",
        "string_literal", "bit_string_literal"
    ]
    for element in sorted(lexical_elements):
        text = re.sub(r" %s([ \n])" % (element), r" %s\1" % (element.upper()), text)
    #
    file = text.split('\n')
    for line in file:
        if len(line) > 0:
           if line[0:1] != " ":
              print (line)
              rule = line[0:len(line)-2]
              opt  = 1
           else:
              print (line + " {")
              print (" "*14 + "print_debug(%s, %d, 0);" % ('"' + rule + '"',opt))
              print (" "*7 + "}")
              opt += 1
        else:
           print (line)

def printStats(text):
    print ("* Stats:")
    results = re.findall(r'([a-z_\d]+) :', text)
    results.sort()
    for res in results:
        uses = re.findall(r'( |\n|__)%s[ \n]' % (res), text)
        num  = len(uses) - 1
        flag = " (not used)" if (num < 1) else ""
        print ("%-50s %3d%s" % (res, num, flag))

# Main ########################################################################

print ("### Code for Flex ###\n")
printFlexCode(text)

print ("\n### Code for Bison ###\n")
text = removeRules(text)
printBisonCode(text)

print ("### Stats ###\n")
printStats(text)
