#!/usr/bin/python
#
# Copyright (C) 2017-2019, Rodrigo A. Melo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys, re

text = sys.stdin.read()

# Deletions
text = re.sub(r"(?m)^;.*\n", "", text)  # comments
text = re.sub(r"\n ", " ", text)        # line jumps
text = re.sub(r"\n{2,}", "\n\n" , text) # multiple line jumps
text = re.sub(r" +", " " , text)        # multiple spaces
text = re.sub(r"(__\w+__)" , r"", text) # __something__

# Add quotes
text = re.sub(r"'"                                , r"'\''"            , text)
text = re.sub(r" (/=|<=|>=|=>|<>|:=|\*\*)([ \n])" , r' "\1"\2'         , text)
text = re.sub(r" (\(|\)|\")([ \n])"               , r" '\1'\2"         , text)
text = re.sub(r" (\\)([ \n])"                     , r" '\1\1'\2"       , text)
text = re.sub(r" ([;#&@+-:*/=<>,.])([ \n])"       , r" '\1'\2"         , text)
# Special cases of VHDL 93
text = re.sub(r"\{ \|"                            , r"{ '|'"           , text)
text = re.sub(r"\[ \[ type_mark"                  , r"'[' [ type_mark" , text)
text = re.sub(r"type_mark \] \]"                  , r"type_mark ] ']'" , text)
text = re.sub(r" ([EBOX])([ \n])"                 , r" '\1'\2"         , text)

# Curly braces ################################################################
# Sharing
num = 1
matches = re.findall(r"\{ ([^}]*) \}",text)
aux = {}
for match in matches:
    if match in aux:
       aux[match] += 1
    else:
       aux[match] = 1
for element in sorted(aux):
    if aux[element] > 1 :
       result = "shared_curly_braces_%02d" % (num)
       text = text.replace("{ %s }" % (element), result)
       text += "\n%s ::= | %s %s\n" % (result, result, element)
       num += 1
# aaa ::= { bbb }
# N1  ::= bbb
# N2  ::= | N2 N1
# aaa ::= N2
#     ::= | N2 N1
#     ::= | aaa bbb
text = re.sub(r"([a-z_]*) ::= \{ ([^}]*) \}\n", r"\1 ::= | \1 \2\n", text)
# aaa ::= bbb { ccc }
# N1  ::= ccc
# N2  ::= | N2 N1
# aaa ::= bbb N2
#     ::= bbb | bbb N2 N1
#     ::= bbb | aaa ccc
text = re.sub(r"([a-z_]*) ::= ([a-z_]*) \{ ([^}]*) \}\n", r"\1 ::= \2 | \1 \3\n", text)
# Other cases
num = 1
matches = re.findall(r"{ (.*?) }",text)
for match in matches:
    result = "curly_braces_%02d" % (num)
    text = text.replace("{ %s }" % (match), result)
    text += "\n%s ::= | %s %s\n" % (result, result, match)
    num+=1

# Square bracket ##############################################################
# Sharing
num = 1
matches = re.findall(r"\[ ([^]]*) \]",text)
aux = {}
for match in matches:
    if match in aux:
       aux[match] += 1
    else:
       aux[match] = 1
for element in sorted(aux):
    if aux[element] > 1 :
       result = "shared_square_brackets_%02d" % (num)
       text = text.replace("[ %s ]" % (element), result)
       text += "\n%s ::= | %s\n" % (result, element)
       num += 1
# Nested Square brackets
# aaa : bbb [ ccc [ ddd ] ] [ eee [ fff ] ] ggg
num = 1
matches = re.findall(r"\[ (.*? \[ .*? \]) \]",text)
for match in matches:
    result = "square_brackets_%02d" % (num)
    text = text.replace("[ %s ]" % (match), result)
    text += "\n%s ::= | %s\n" % (result, match)
    num+=1
# Otehrs -> aaa : bbb [ ccc ] ddd [ eee ] [ fff ] ggg
matches = re.findall(r"\[ (.*?) \]",text)
for match in matches:
    result = "square_brackets_%02d" % (num)
    text = text.replace("[ %s ]" % (match), result)
    text += "\n%s ::= | %s\n" % (result, match)
    num+=1

# Finish ######################################################################S
print (text)
